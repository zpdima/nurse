package cf.zpdima.nurse.domain;

import cf.zpdima.nurse.domain.enums.VitalSignsSource;
import cf.zpdima.nurse.dto.View;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDateTime;

@Data
public class VitalSignsData  extends VitalSignsDictionary {

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private Long pk;

    @JsonView({View.VitalsEdit.class})
    @NotNull
    private Long vitalSignsDictionaryFk;

    @JsonView({View.VitalsEdit.class})
    @NotNull
    private Integer patientFk;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private VitalSignsSource source;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private String measuring;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private String notes;

    @JsonView({View.VitalsList.class})
    private Integer userFk;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    @Past
    private LocalDateTime indicatorTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    @JsonView({View.VitalsList.class})
    private LocalDateTime modifiedTime;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    @NotNull
    private Float value1;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private Float value2;


}
