package cf.zpdima.nurse.domain.enums;

public enum VitalSignsType {
    DECIMAL, OBJECT;
}
