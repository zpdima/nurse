package cf.zpdima.nurse.domain.enums;

public enum VitalSignsSource {
    MANUAL, SENSOR;
}
