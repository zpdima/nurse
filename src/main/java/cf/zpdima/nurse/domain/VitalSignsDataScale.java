package cf.zpdima.nurse.domain;

import cf.zpdima.nurse.dto.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

@Data
public class VitalSignsDataScale {

    private Long       pk;

    private Long       vitalSignsDictionaryFk;

    private Integer    notificationFk;

    @JsonView({View.VitalsList.class})
    private String     styleClass;

    private Byte       priority;

    private Float criticalValue1Start;

    private Float criticalValue1End;

    private Float criticalValue2Start;

    private Float criticalValue2End;

}
