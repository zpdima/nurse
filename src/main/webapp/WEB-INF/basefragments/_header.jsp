<%@ page import="java.util.Locale" %>

<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="/"><fmt:message key="header.home"/></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/patients"><fmt:message key="header.patients"/></a>
                <!--                <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>-->
            </li>
            <!--            <li class="nav-item">-->
            <!--                <a class="nav-link" href="#">Link</a>-->
            <!--            </li>-->
            <!--                        <li class="nav-item dropdown">-->
            <!--                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
            <!--                                Dropdown-->
            <!--                            </a>-->
            <!--                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">-->
            <!--                                <a class="dropdown-item" href="#">Action</a>-->
            <!--                                <a class="dropdown-item" href="#">Another action</a>-->
            <!--                                <div class="dropdown-divider"></div>-->
            <!--                                <a class="dropdown-item" href="#">Something else here</a>-->
            <!--                            </div>-->
            <!--                        </li>-->
            <!--            <li class="nav-item">-->
            <!--                <a class="nav-link disabled" href="#">Disabled</a>-->
            <!--            </li>-->
        </ul>

        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit"><fmt:message key="header.search"/></button>
        </form>

<%--                <%--%>
<%--                    Locale l = request.getLocale();--%>
<%--                    out.println("LOCALE :"+l+"country "+l.getDisplayCountry());--%>
<%--                %>--%>


        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">

                <c:set var="localeCode" value="${pageContext.response.locale}"/>
<%--                ${localeCode}--%>
                <c:choose>
                    <c:when test="${localeCode == 'de_DE' || localeCode == 'de'}">
                        <c:set value="flag-icon flag-icon-de" var="cssLangClass"></c:set>
                    </c:when>
                    <c:when test="${localeCode == 'ru_RU' || localeCode == 'ru'}">
                        <c:set value="flag-icon flag-icon-ru" var="cssLangClass"></c:set>
                    </c:when>
                    <c:when test="${localeCode == 'en_EN' || localeCode == 'en' }">
                        <c:set value="flag-icon flag-icon-gb" var="cssLangClass"></c:set>
                    </c:when>
                    <c:when test="${localeCode == 'pl_PL' || localeCode == 'pl' }">
                        <c:set value="flag-icon flag-icon-pl" var="cssLangClass"></c:set>
                    </c:when>
                    <c:when test="${localeCode == 'fr_FR' || localeCode == 'fr' }">
                        <c:set value="flag-icon flag-icon-fr" var="cssLangClass"></c:set>
                    </c:when>
                    <c:when test="${localeCode == 'pr_PR' || localeCode == 'pr' }">
                        <c:set value="flag-icon flag-icon-pr" var="cssLangClass"></c:set>
                    </c:when>
                    <c:when test="${localeCode == 'ua_UA' || localeCode == 'ua' }">
                        <c:set value="flag-icon flag-icon-ua" var="cssLangClass"></c:set>
                    </c:when>

                    <c:otherwise>
                        <c:set value="flag-icon flag-icon-ru" var="cssLangClass"></c:set>
                    </c:otherwise>
                </c:choose>


                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        <span class="${cssLangClass}">${localecode}</span>
                        <span><%= response.getLocale().getDisplayLanguage(request.getLocale())%></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdown09">
                        <a class="dropdown-item" href="?lang=en"><span class="flag-icon flag-icon-gb"> </span>
                            English</a>
                        <a class="dropdown-item" href="?lang=de"><span class="flag-icon flag-icon-de"> </span>
                            Deutsch</a>
                        <a class="dropdown-item" href="?lang=fr"><span class="flag-icon flag-icon-fr"> </span>
                            Le français</a>
                        <a class="dropdown-item" href="?lang=pl"><span class="flag-icon flag-icon-pl"> </span>
                            Português</a>
                        <a class="dropdown-item" href="?lang=pr"><span class="flag-icon flag-icon-pr"> </span>
                            Português</a>
                        <a class="dropdown-item" href="?lang=ru"><span class="flag-icon flag-icon-ru"> </span>
                            Русский</a>
                        <a class="dropdown-item" href="?lang=ua"><span class="flag-icon flag-icon-ua"> </span>
                            Український</a>

                    </div>
                </li>

                <sec:authorize access="isAnonymous()">
                    <li class="nav-item">
                        <a class="nav-link" href="/login"><fmt:message key="header.login"/></a>
                    </li>
                </sec:authorize>
                <sec:authorize access="!isAnonymous()">
                    <li class="nav-item">
                        <a class="nav-link" href="/logout"><fmt:message key="header.logout"/> '${pageContext.request.userPrincipal.name}' </a>
                    </li>
                </sec:authorize>

            </ul>
        </div>
    </div>
</nav>
