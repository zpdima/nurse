ALTER TABLE `patient_vital_signs` ADD CONSTRAINT `fk_pvs_patients` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`);

ALTER TABLE `patient_vital_signs` ADD CONSTRAINT `fk_pvs_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

