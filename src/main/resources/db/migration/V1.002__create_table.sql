CREATE TABLE IF NOT EXISTS users (
                                     username VARCHAR(25) NOT NULL,
                                     password VARCHAR(256) NOT NULL,
                                     enabled TINYINT NOT NULL DEFAULT 1,
					PRIMARY KEY (username)
);

CREATE TABLE IF NOT EXISTS authorities (
                                           username VARCHAR(25) NOT NULL,
                                           authority VARCHAR(25) NOT NULL,
                                           FOREIGN KEY (username) REFERENCES users(username)
);

 
CREATE UNIQUE INDEX ix_auth_username
  on authorities (username,authority);



-- The encrypted password is `pass`
INSERT INTO users (username, password, enabled) VALUES ('user', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);
INSERT INTO authorities (username, authority) VALUES ('user', 'ROLE_USER');
INSERT INTO authorities (username, authority) VALUES ('user', 'ROLE_VIEWER');


INSERT INTO users (username, password, enabled) VALUES ('adm', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);
INSERT INTO authorities (username, authority) VALUES ('adm', 'ROLE_ADMIN');
INSERT INTO authorities (username, authority) VALUES ('adm', 'ROLE_USER');

