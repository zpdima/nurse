package cf.zpdima.nurse.services.impl;

import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.nurse.dto.PatientDTO;
import cf.zpdima.nurse.repository.PatientRepository;
import cf.zpdima.nurse.services.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.nio.file.AccessDeniedException;
import java.util.List;

@Service
public class PatientServiceImpl implements PatientService {

    private final Logger logger = LoggerFactory.getLogger(PatientServiceImpl.class);

    @Autowired
    PatientRepository patientRepository;

    public Patient findById(Long id) {
        return patientRepository.findById(id);    }

//    @PreAuthorize("hasRole('NURSE')")
    public List<Patient> getByNursePatients(String nurse) {
        return patientRepository.getByNursePatients(nurse);
    }

//    @PreAuthorize("hasAnyRole({'NURSEADMIN'})")
    public List<Patient> getPatients() {
        return patientRepository.getPatients();
    }


    @PreAuthorize("hasAnyRole({'NURSEADMIN','NURSE'})")
    public int insert(Patient patient){
        return patientRepository.insert(patient);
    }

//    @PreAuthorize("hasAnyRole({'NURSEADMIN','NURSE'})")
//    @PreAuthorize("hasAnyRole({'NURSEADMIN','NURSE'}) and #patientDTO.nurse == authentication.principal.username")
    @PreAuthorize("hasAnyRole({'NURSEADMIN'}) " +
            "or (hasAnyRole({'NURSE'}) and #patientDTO.nurse == authentication.principal.username)")
    public int update(PatientDTO patientDTO){
        Patient patient = findById(patientDTO.getId());
        patient.setAddress(patientDTO.getAddress());
        patient.setBirthDate(new java.sql.Date(patientDTO.getBirthDate().getTime()));
        patient.setReceiptDate(new java.sql.Date(patientDTO.getReceiptDate().getTime()));
        patient.setComplaints(patientDTO.getComplaints());
        patient.setDiagnosis(patientDTO.getDiagnosis());
        patient.setEmail(patientDTO.getEmail());
        patient.setFloor(patientDTO.getFloor());
        patient.setFullName(patientDTO.getFullName());
        patient.setHousing(patientDTO.getHousing());
        patient.setPhones(patientDTO.getPhones());
        patient.setNurse(patientDTO.getNurse());
        patient.setCardStatus(patientDTO.getCardStatus());
        logger.info(" --- email: {}", patient.getEmail());
        return patientRepository.update(patient);
    }

//    @PreAuthorize("hasRole('NURSE')")
    @PreAuthorize("hasAnyRole({'NURSEADMIN','NURSE'}) and #patient.nurse == authentication.principal.username")
    public int update(Patient patient){
        return patientRepository.update(patient);
    }

    @PreAuthorize("hasRole('NURSE')")
    public int delete(Patient patient){
        return patientRepository.delete(patient);
    }

    @PreAuthorize("hasAnyRole({'NURSEADMIN','NURSE'}) and #username == authentication.principal.username")
    public int delete(Long id, String username) throws AccessDeniedException {
        logger.info(" --- delete id {} user {}", id, username);
        return patientRepository.delete(id);
    }

    @PreAuthorize("hasRole('NURSE')")
    public int delete(Long id){
        logger.info(" --- delete id {}", id);
        return patientRepository.delete(id);
    }

}
