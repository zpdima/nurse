package cf.zpdima.nurse.controllers;


import java.nio.file.AccessDeniedException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import cf.zpdima.nurse.domain.*;
import cf.zpdima.nurse.domain.enums.VitalSignsSource;
import cf.zpdima.nurse.dto.PatientDTO;
import cf.zpdima.nurse.dto.View;
import cf.zpdima.nurse.repository.PatientVitalSignsRepository;
import cf.zpdima.nurse.repository.VitalSignsDAO;
import cf.zpdima.nurse.services.BookService;
import cf.zpdima.nurse.services.impl.PatientServiceImpl;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.acls.model.NotFoundException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
@Slf4j
public class ApiController {
    private final Logger logger = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    BookService bookService;

    @Autowired
    PatientServiceImpl patientService;

    @Autowired
    PatientVitalSignsRepository patientVitalSignsRepository;

    @Autowired
    VitalSignsDAO vitalSignsDAO;

    @Autowired
    ConversionService conversionService;


    @GetMapping(value = "/books", produces = "application/json")
    @ResponseBody
    public List<Book> getBooks() {
        return this.bookService.getBooks();
    }

    @GetMapping(value = "/patients", produces = "application/json")
    @ResponseBody
    public List<PatientDTO> getPaties() {
        return this.patientService.getPatients().stream().map(p -> conversionService.convert(p, PatientDTO.class)).collect(Collectors.toList());
    }

    @GetMapping(value = "/patients/{id}", produces = "application/json")
    @ResponseBody
//    @PreAuthorize("hasRole('NURSE') and #username == authentication.principal.username")
    public PatientDTO getPatiesId(@PathVariable Long id,
                                  Locale locale) {
        logger.info(" --- /patients/{id} locale: {}", locale );
        logger.info(this.patientService.findById(id).toString());
        logger.info(conversionService.convert(this.patientService.findById(id), PatientDTO.class).toString());
        PatientDTO patientDTO = conversionService.convert(this.patientService.findById(id), PatientDTO.class);
        logger.info(" --- status {} {}", patientDTO.getCardStatus() , patientDTO.getCardStatus().toString());
        logger.info(" --- status {} ", conversionService.convert(patientDTO.getCardStatus(), String.class));
        return conversionService.convert(this.patientService.findById(id), PatientDTO.class);
    }


    @PostMapping(path = "/patients", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> insertPatient(@RequestBody @Valid PatientDTO patientDTO,
                                           HttpServletRequest request,
                                           HttpServletResponse response,
                                           Locale locale) {
        //code
        logger.info(" --- POST patient : {} locale: {} - {} - {}", patientDTO, request.getLocale(), response.getLocale(), locale );
        int r = 0;
        try {

//            try {
//                Thread.sleep(7000);
//            } catch (InterruptedException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }

//            if(1 ==1) {
//                throw new Exception(" !!! test Exception !!!");
//            }
            int a = conversionService.convert("25", Integer.class);

            Patient patient = conversionService.convert(patientDTO, Patient.class);
            r = patientService.insert(patient);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        logger.info(" --- POST r {}", r);
        return ResponseEntity.ok(r);
    }

    @PutMapping(path = "/patients", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> updatePatient(@RequestBody @Valid PatientDTO patientDTO) {
        //code
        logger.info(" --- PUT patient : {}", patientDTO);
        int r = 0;
        try {
            r = patientService.update(patientDTO);
        } catch (Exception e) {
            logger.error(" --- Error:", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        logger.info(" --- PUT r {}", r);
        return ResponseEntity.ok(r);
    }

    @DeleteMapping(value = "/patients/{id}")
//    @PreAuthorize("hasRole('NURSE') and #username == authentication.principal.username")
    public ResponseEntity<?> deletPatient(@PathVariable Long id, String username) throws AccessDeniedException {
        //code
        logger.info(" --- DELETE patient is : {}", id);

        Patient patient = patientService.findById(id);
        if(patient == null){
            throw new NotFoundException("Not Found");
        }
        logger.info(" --- DELETE patient is : {} nurse {}", id);

        int r = 0;
//        try {
            r = patientService.delete(id, patient.getNurse());
//        } catch (Exception e) {
////            throws e;
////            return ResponseEntity.badRequest().body(e.getMessage());
//        }
        logger.info(" --- DELETE r {}", r);
        return ResponseEntity.ok(r);
    }


    @GetMapping(value = "/patient-vital-signs", produces = "application/json")
    @ResponseBody
    public List<PatientVitalSigns> getPatientVitalSigns() {
        return this.patientVitalSignsRepository.getPatientVitalSigns();
    }

    @PostMapping(path = "/patient-vital-signs", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> savePatientVitalSigns(@RequestBody @Valid PatientVitalSigns patientVitalSigns,
                                           HttpServletRequest request,
                                           HttpServletResponse response,
                                           Locale locale) {
        //code
        Long r = 0l;
        try {
            r = patientVitalSignsRepository.save(patientVitalSigns);
//            r = patientVitalSignsRepository.insert(patientVitalSigns);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }

        logger.info(" --- POST r {}", r);
        return ResponseEntity.ok(r);
    }


    @GetMapping(value = "/vital-signs-data", produces = "application/json")
    @JsonView(View.VitalsList.class)
    public List<VitalSignsData> getVitalSignsDataByPatient(@RequestParam(name = "patientId") Long patientId,
                                                           @RequestParam(name = "dtb") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dtb,
                                                           @RequestParam(name = "dte") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dte,
                                                           HttpServletRequest request,
                                                           HttpServletResponse response,
                                                           Locale locale) {

        return vitalSignsDAO.findVitalSignsDataAndStyleByPatientFkAndIndicatorTime(patientId, dtb, dte);

    }

    @GetMapping(value = "/vital-signs-data-list-for-insert", produces = "application/json")
    @JsonView(View.VitalsEdit.class)
    public List<VitalSignsData> lisVitalSignsDataForEdit(@RequestParam(name = "enabled") Byte enabled,
                                                         HttpServletRequest request,
                                                         HttpServletResponse response,
                                                         Locale locale) {

        List<VitalSignsDictionary> listDictionary =  vitalSignsDAO.findVitalSignsDictionaryByEnabled(enabled);
        List<VitalSignsData> listData = listDictionary.stream().map(dictionary -> {
            VitalSignsData vitalSignsData = new VitalSignsData( );
            vitalSignsData.setSource(VitalSignsSource.MANUAL);
            vitalSignsData.setType(dictionary.getType());
            vitalSignsData.setUnit(dictionary.getUnit());
            vitalSignsData.setVitalSignsDictionaryFk(dictionary.getPk());
            vitalSignsData.setName(dictionary.getName());
            return vitalSignsData;
        } ).collect(Collectors.toList());
        return listData;
    }

    @PostMapping(path = "/vital-signs-data", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> saveVitalSignsData(@RequestBody @Valid VitalSignsData vitalSignsData,
                                                HttpServletRequest request,
                                                HttpServletResponse response,
                                                Locale locale) {

        log.info(" --- vitalSignsData {}", vitalSignsData.getValue1());
        Map<String, Object> r = new HashMap<>();
        try {

//            Patient patient = conversionService.convert(patientDTO, Patient.class);
//            r.put("pk", vitalSignsDAO.insert(vitalSignsData));
            r.put("pk", vitalSignsDAO.save(vitalSignsData));
        } catch (Exception e) {
            log.info(" --- e {}", e);
//            return ResponseEntity.badRequest().body(e.getMessage());
            r.put("pk", -1l);
            r.put("errorMessage", e.getMessage());
            r.put("errorCause", e.getCause());
            return ResponseEntity.badRequest().body(r);
        }

        return ResponseEntity.ok(r);
//        return ResponseEntity.ok(Collections.singletonMap("pk", pk));
    }


}
