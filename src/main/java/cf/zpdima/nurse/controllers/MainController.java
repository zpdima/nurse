package cf.zpdima.nurse.controllers;

import cf.zpdima.nurse.domain.Authority;
import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.nurse.domain.enums.CardPatientStatus;
import cf.zpdima.nurse.dto.PatientDTO;
import cf.zpdima.nurse.services.AuthotitytService;
import cf.zpdima.nurse.services.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.security.RolesAllowed;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Controller
public class MainController {

    private final Logger logger = LoggerFactory.getLogger(MainController.class);

    @Autowired
    PatientService patientService;

    @Autowired
    AuthotitytService authotitytService;


    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String homePage(Model model) {
        return "homePage";
    }

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String loginPage(Model model) {
        return "login";
    }

    @RequestMapping(value = {"/login-error"}, method = RequestMethod.GET)
    public String loginErrorPage(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }




    @RequestMapping(value = {"/contactus"}, method = RequestMethod.GET)
    public String contactusPage(Model model) {
        model.addAttribute("app", true);
        model.addAttribute("address", "Zaporigya");
        model.addAttribute("phone", "+380689098703");
        model.addAttribute("email", "zpdima2017@gmail.com");
        return "contactusPage";
    }

    @RequestMapping(value = {"/patients"}, method = RequestMethod.GET)
//    @RolesAllowed({"ROLE_NURSE", "ROLE_NURSEADMIN"})
//    @PreAuthorize("hasRole('ROLE_VIEWER')")
    public String patientsPage(Model model,HttpServletRequest request,
                               HttpServletResponse response,
                               Locale locale) {

        logger.info(" --- patients  locale: {} - {} - {}",  request.getLocale(), response.getLocale(), locale );


        List<Patient> list = new ArrayList<>();

        if (request.getUserPrincipal() != null && request.isUserInRole("NURSE")) {
//            list = patientService.getByNursePatients(request.getUserPrincipal().getName());
            list = patientService.getPatients();
        }
        if (request.getUserPrincipal() != null && request.isUserInRole("NURSEADMIN")) {
            list = patientService.getPatients();
        }
//        list = patientService.getPatients();
        model.addAttribute("patients", list);

        return "patientsPage";
    }

//    @RequestMapping(value = { "/edit-patient/{id}" }, params = "form",  method = RequestMethod.GET)
//    public String editPatientPage(@PathVariable("id") Long id, Model model) {
//        logger.info(" --- Edit patient id = {}", id);
//
////        Patient patient = patientService.
//
//        model.addAttribute("patient", "aaa");
//
//        return "editPage";
//    }

    @RequestMapping(value = "/edit-patient", method = RequestMethod.GET)
    public String edit1PatientPage(@RequestParam(name = "id") Long id, Model model) {
        logger.info(" --- Edit patient id = {}", id);

        Patient patient = patientService.findById(id);

//        patient.getCardStatus().getClass().value
        List listStatus = Arrays.stream(CardPatientStatus.values()).collect(Collectors.toList());
        List listNurse = authotitytService.getAuthoritiesNurse();
        listNurse.add(0, new Authority());

        model.addAttribute("patient", patient);
        model.addAttribute("listStatus", listStatus);
        model.addAttribute("listNurse", listNurse);

        return "editPage";
    }

    @RequestMapping(value = "/add-patient", method = RequestMethod.GET)
    public String edit1PatientPage(Model model, HttpServletRequest request, HttpServletResponse response) {

        logger.info(" --- Add patient ");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        Patient patient = new Patient();
        patient.setReceiptDate(new java.sql.Date(Calendar.getInstance().getTimeInMillis()));
        patient.setCardStatus(CardPatientStatus.NEW);
        if (request.getUserPrincipal() != null) {
            patient.setNurse(request.getUserPrincipal().getName());
        }

        List listStatus = Arrays.stream(CardPatientStatus.values()).collect(Collectors.toList());
        List listNurse = authotitytService.getAuthoritiesNurse();

        model.addAttribute("patient", patient);
        model.addAttribute("listStatus", listStatus);
        model.addAttribute("listNurse", listNurse);


        model.addAttribute("patient", patient);

        return "editPage";
    }

    @RequestMapping(value = {"/table"}, method = RequestMethod.GET)
    public String tablePage(Model model) {
        model.addAttribute("app", true);
        model.addAttribute("address", "Zaporigya");
        model.addAttribute("phone", "+380689098703");
        model.addAttribute("email", "zpdima2017@gmail.com");
        return "tablePage";
    }

}
