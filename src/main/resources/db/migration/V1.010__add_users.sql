INSERT INTO users(username, password, enabled) VALUES ('nurse1', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);
INSERT INTO users(username, password, enabled) VALUES ('nurse2', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);
INSERT INTO users(username, password, enabled) VALUES ('nurse3', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);
INSERT INTO users(username, password, enabled) VALUES ('nurse4', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);


INSERT INTO authorities(username, authority) VALUES ('nurse1', 'ROLE_NURSE');
INSERT INTO authorities(username, authority) VALUES ('nurse2', 'ROLE_NURSE');
INSERT INTO authorities(username, authority) VALUES ('nurse3', 'ROLE_NURSE');
INSERT INTO authorities(username, authority) VALUES ('nurse4', 'ROLE_NURSE');

