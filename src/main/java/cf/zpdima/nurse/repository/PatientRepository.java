package cf.zpdima.nurse.repository;

import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.spring.jooq.sample.model.Tables;
import org.jooq.DSLContext;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientRepository {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    DSLContext dsl;

    public List<Patient> getPatients() {
        return dsl
                .selectFrom(Tables.PATIENTS)
                .fetch()
                .stream()
                .map(e -> mapper.map(e, Patient.class))
                .collect(Collectors.toList());
    }

    public List<Patient> getByNursePatients(String nurse) {
        return dsl
                .selectFrom(Tables.PATIENTS)
                .where(Tables.PATIENTS.NURSE.equal(nurse))
                .fetch()
                .stream()
                .map(e -> mapper.map(e, Patient.class))
                .collect(Collectors.toList());
    }


    public Patient findById(Long id) {
        return mapper.map(dsl.selectFrom(Tables.PATIENTS).
                where(Tables.PATIENTS.ID.equal(id)).fetchOne(), Patient.class);
    }

    public int insert(Patient patient) {

        return dsl.insertInto(Tables.PATIENTS)
                .set(Tables.PATIENTS.USER_ID, patient.getUserId())
                .set(Tables.PATIENTS.ADDRESS, patient.getAddress())
                .set(Tables.PATIENTS.BIRTH_DATE, patient.getBirthDate())
                .set(Tables.PATIENTS.COMPLAINTS, patient.getComplaints())
                .set(Tables.PATIENTS.DIAGNOSIS, patient.getDiagnosis())
                .set(Tables.PATIENTS.FLOOR, patient.getFloor())
                .set(Tables.PATIENTS.FULL_NAME, patient.getFullName())
                .set(Tables.PATIENTS.HOUSING, patient.getHousing())
                .set(Tables.PATIENTS.PHONES, patient.getPhones())
                .set(Tables.PATIENTS.RECEIPT_DATE, patient.getReceiptDate())
                .set(Tables.PATIENTS.WARD, patient.getWard())
                .set(Tables.PATIENTS.NURSE, patient.getNurse())
                .set(Tables.PATIENTS.CARD_STATUS, patient.getCardStatus().toString())
                .execute();
//        return patient;
    }

    public int update(Patient patient) {

        return dsl.update(Tables.PATIENTS)
//                .set(Tables.PATIENTS.ID, patient.getId())
                .set(Tables.PATIENTS.USER_ID, patient.getUserId())
                .set(Tables.PATIENTS.ADDRESS, patient.getAddress())
                .set(Tables.PATIENTS.BIRTH_DATE, patient.getBirthDate())
                .set(Tables.PATIENTS.COMPLAINTS, patient.getComplaints())
                .set(Tables.PATIENTS.DIAGNOSIS, patient.getDiagnosis())
                .set(Tables.PATIENTS.FLOOR, patient.getFloor())
                .set(Tables.PATIENTS.FULL_NAME, patient.getFullName())
                .set(Tables.PATIENTS.HOUSING, patient.getHousing())
                .set(Tables.PATIENTS.PHONES, patient.getPhones())
                .set(Tables.PATIENTS.RECEIPT_DATE, patient.getReceiptDate())
                .set(Tables.PATIENTS.WARD, patient.getWard())
                .set(Tables.PATIENTS.NURSE, patient.getNurse())
                .set(Tables.PATIENTS.CARD_STATUS, patient.getCardStatus().toString())
                .where(Tables.PATIENTS.ID.equal(patient.getId()))
                .execute();
//        return patient;
    }

    public int delete(Patient patient) {

        return dsl.delete(Tables.PATIENTS)
                .where(Tables.PATIENTS.ID.equal(patient.getId()))
                .execute();
//        return patient;
    }

    public int delete(Long id) {

        return dsl.delete(Tables.PATIENTS)
                .where(Tables.PATIENTS.ID.equal(id))
                .execute();
//        return patient;
    }


}
