package cf.zpdima.nurse.domain;

import cf.zpdima.nurse.domain.enums.CardPatientStatus;
import lombok.*;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.Calendar;

@Data
public class Patient {


    private Long id;
    private Long userId;
    private String address;
    private Date birthDate;
    private String fullName;
    private String phones;
    private Date receiptDate;
    private String complaints;
    private String diagnosis;
    private Long housing;
    private Long floor;
    private Long ward;
    private String email;
    private String secret;
    private String nurse;
    private CardPatientStatus cardStatus;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public CardPatientStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardPatientStatus cardStatus) {
        this.cardStatus = cardStatus;
    }

    public void setCardStatus(String strCardStatus) {
        this.cardStatus = CardPatientStatus.valueOf(strCardStatus);
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }

    public Date getReceiptDate() {
        return receiptDate;
    }

    public void setReceiptDate(Date receiptDate) {
        this.receiptDate = receiptDate;
    }

    public String getComplaints() {
        return complaints;
    }

    public void setComplaints(String complaints) {
        this.complaints = complaints;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Long getHousing() {
        return housing;
    }

    public void setHousing(Long housing) {
        this.housing = housing;
    }

    public Long getFloor() {
        return floor;
    }

    public void setFloor(Long floor) {
        this.floor = floor;
    }

    public Long getWard() {
        return ward;
    }

    public void setWard(Long ward) {
        this.ward = ward;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getNurse() {
        return nurse;
    }

    public void setNurse(String nurse) {
        this.nurse = nurse;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", birthDate=" + birthDate +
                ", fullName='" + fullName + '\'' +
                ", phones='" + phones + '\'' +
                ", email='" + email + '\'' +
                ", receiptDate=" + receiptDate +
                ", complaints='" + complaints + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", housing=" + housing +
                ", floor=" + floor +
                ", ward=" + ward +
                ", secret=" + secret +
                ", nurse=" + nurse +
                ", cardStatus=" + cardStatus +
                '}';
    }
}
