package cf.zpdima.nurse.services.impl;

import cf.zpdima.nurse.domain.Authority;
import cf.zpdima.nurse.repository.AuthorityRepository;
import cf.zpdima.nurse.services.AuthotitytService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthotitytServiceImpl implements AuthotitytService {

    @Autowired
    private AuthorityRepository authorityRepository;

    @Override
    public List<Authority> getAuthoritiesNurse() {
        return authorityRepository.getAuthoritiesNurse();
    }
}
