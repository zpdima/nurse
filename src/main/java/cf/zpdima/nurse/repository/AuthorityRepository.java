package cf.zpdima.nurse.repository;

import cf.zpdima.nurse.domain.Authority;
import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.spring.jooq.sample.model.Tables;
import org.jooq.DSLContext;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorityRepository {

    @Autowired
    private ModelMapper mapper;

    @Autowired
    DSLContext dsl;

    public List<Authority> getAuthoritiesNurse() {
        return dsl
                .selectFrom(Tables.AUTHORITIES)
                .where(Tables.AUTHORITIES.AUTHORITY.equal("ROLE_NURSE"))
                .fetch()
                .stream()
                .map(e -> mapper.map(e, Authority.class))
                .collect(Collectors.toList());
    }


}
