package cf.zpdima.nurse.formatters;

import cf.zpdima.nurse.domain.I18n;
import cf.zpdima.nurse.domain.enums.CardPatientStatus;
import cf.zpdima.nurse.repository.I18nRepository;
import cf.zpdima.nurse.repository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

public class CardPatientStatusFormatter implements Formatter<CardPatientStatus> {

    private static Logger logger =
            LoggerFactory.getLogger(I18nRepository.class);


    @Autowired
    I18nRepository i18nRepository;

    public I18nRepository getI18nRepository() {
        return i18nRepository;
    }

    public void setI18nRepository(I18nRepository i18nRepository) {
        this.i18nRepository = i18nRepository;
    }

    @Override
    public CardPatientStatus parse(String s, Locale locale) throws ParseException {

        if (i18nRepository == null) {
//            System.out.println("!!!!!!!!!!!!!!! i18nRepository :" +  i18nRepository);
            return null;
        }
        logger.info(" --- parse({}, {}, {})", CardPatientStatus.class.getName(),
                s,
                locale.getLanguage());

        I18n i18n = i18nRepository.findByNameAndKeyAndLang(CardPatientStatus.class.getName(),
                locale.getLanguage().toString(), s);

        if (i18n == null) {
            return null;
        } else {
            CardPatientStatus cardPatientStatus = CardPatientStatus.valueOf(i18n.getKey());
            logger.info(" --- cardPatientStatus : {}", cardPatientStatus);
            return cardPatientStatus;
        }
//        return CardPatientStatus.valueOf(s);
    }

    @Override
    public String print(CardPatientStatus cardPatientStatus, Locale locale) {


        if (i18nRepository == null) {
//            System.out.println("!!!!!!!!!!!!!!! i18nRepository :" +  i18nRepository);
            return cardPatientStatus.toString();
        }

        logger.info(" --- print({}, {}, {})", cardPatientStatus.getClass().getName(),
                cardPatientStatus.toString(),
                locale.getLanguage());

        I18n i18n = i18nRepository.findByNameAndKeyAndLang(cardPatientStatus.getClass().getName(),
                cardPatientStatus.toString(),
                locale.getLanguage().toString());
        if (i18n == null) {
            return cardPatientStatus.toString();
        } else {
            return i18n.getValue();
        }
//        return cardPatientStatus.toString() + " locale " + locale.getLanguage() + cardPatientStatus.getClass().toString();
    }
}
