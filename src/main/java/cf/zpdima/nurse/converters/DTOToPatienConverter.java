package cf.zpdima.nurse.converters;

import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.nurse.dto.PatientDTO;
import org.springframework.core.convert.converter.Converter;


public class DTOToPatienConverter implements Converter<PatientDTO, Patient> {


    @Override
    public Patient convert(PatientDTO patientDTO) {
        Patient patient = new Patient();
        patient.setId(patientDTO.getId());
        patient.setAddress(patientDTO.getAddress());
        patient.setBirthDate(new java.sql.Date(patientDTO.getBirthDate().getTime()));
        patient.setReceiptDate(new java.sql.Date(patientDTO.getReceiptDate().getTime()));
        patient.setComplaints(patientDTO.getComplaints());
        patient.setDiagnosis(patientDTO.getDiagnosis());
        patient.setEmail(patientDTO.getEmail());
        patient.setFloor(patientDTO.getFloor());
        patient.setFullName(patientDTO.getFullName());
        patient.setHousing(patientDTO.getHousing());
        patient.setPhones(patientDTO.getPhones());
        patient.setWard(patientDTO.getWard());
        patient.setNurse(patientDTO.getNurse());
        patient.setCardStatus(patientDTO.getCardStatus());

        return patient;
    }
}
