package cf.zpdima.nurse.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {


    private final DataSource dataSource;

    private PasswordEncoder passwordEncoder;
    private UserDetailsService userDetailsService;

    public WebSecurityConfiguration(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

//    @Override
//    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
//        auth.userDetailsService(userDetailsService())
//                .passwordEncoder(passwordEncoder());
//    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery("select username,password,enabled "
                        + "from users "
                        + "where username = ?")
                .authoritiesByUsernameQuery("select u.username, a.authority "
                        + "from authorities a LEFT JOIN users u on a.user_id = u.id "
                        + "where u.username = ?");
    }

//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        if (passwordEncoder == null) {
//            passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
//        }
//        return passwordEncoder;
//    }

//    @Bean
//    public UserDetailsService userDetailsService() {
//        if (userDetailsService == null) {
//            userDetailsService = new JdbcDaoImpl();
//            ((JdbcDaoImpl) userDetailsService).setDataSource(dataSource);
//        }
//        return userDetailsService;
//    }


    @Override
    protected void configure(HttpSecurity httpSecurity)
            throws Exception {
        httpSecurity.authorizeRequests()
                .antMatchers("/h2-console/**")
                .permitAll()

                .antMatchers("/patients123").hasAnyRole(new String[]{"NURSE", "NURSEADMIN"})
                .antMatchers("/patients1")
                .authenticated()

                .antMatchers("/**")
                .permitAll()
//                .hasAnyRole(new String[]{"NURSE", "NURSEADMIN", "USER", "ADMIN"})

                .anyRequest()
                .authenticated()
                .and()
//                .formLogin();
                .formLogin()
                .loginPage("/login")
                .failureUrl("/login-error")

                .defaultSuccessUrl("/")
                .and()
                .logout()
//                .logoutUrl("/logout")
                .logoutSuccessUrl("/")
                .and().csrf().disable();

//        httpSecurity.csrf()
//                .ignoringAntMatchers("/h2-console/**");
        httpSecurity.headers()
                .frameOptions()
                .sameOrigin();


    }
}
