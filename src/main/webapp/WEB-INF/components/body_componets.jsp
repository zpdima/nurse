<%--
  Created by IntelliJ IDEA.
  User: Dima
  Date: 18.12.2019
  Time: 10:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%--<hr/>--%>
<%--<div>--%>
<%--    ${pageContext.request.userPrincipal.name}--%>
<%--    <!-- or -->--%>
<%--    ${pageContext.request.remoteUser}--%>
<%--    <c:set var="username" value="${pageContext.request.remoteUser}"/>--%>
<%--    ${username}--%>
<%--</div>--%>

<%--<hr/>--%>

<!-- !!! part componets start  !!!  -->

<style>
    #app-2, #app-3 {
        border: 1px dotted aquamarine;
        padding: 15px;

    }
</style>


<c:if test="${app}">
    <div id="app">
        {{ message }}
    </div>
    <script>
        var app = new Vue({
            el: '#app',
            data: {
                message: 'Привет, Vue!'
            }
        })
    </script>
</c:if>

<c:if test="${app1111111111}">
    <div id="app-2" v-if="seen">

            <%--  <span v-bind:title="message">--%>
            <%--    Наведи на меня курсор на пару секунд,--%>
            <%--    чтобы увидеть динамически связанное значение title!--%>
            <%--  </span>--%>
            <%--    <div>--%>
            <%--        <span>Name: {{ customer.name }}</span>--%>
            <%--        Age: {{ customer.age }}--%>
            <%--    </div>--%>

        <div style="background: #7abaff; padding: 7px 0;">Patient: {{ patient.fullName }}</div>
        <div>

            <div>Address: {{ patient.address }}</div>
            <div>birthDate: {{ patient.birthDate }}</div>
            <div>phones: {{ patient.phones }}</div>
            <div>receiptDate: {{ patient.receiptDate }}</div>
            <div>complaints: {{ patient.complaints }}</div>
            <div>nurse: {{ patient.nurse }}</div>
        </div>

    </div>


    <script>

        var app = new Vue({
            el: '#app',
            data: {
                message: 'Привет, Vue!'
            }
        })


        var app2 = new Vue({
            el: '#app-2',
            data: {
                message: 'Вы загрузили эту страницу: ' + new Date().toLocaleString(),
                seen: false,
                customer: {
                    name: 'Вася',
                    age: 37
                },
                patient: {
                    address: '',
                    fullName: '',
                }

            },

            // mounted() {
            //     axios
            //         .get('/api/v1/patients/1')
            //         .then(response => (this.info = response));
            // }

            // created(){
            //     let self = this;
            //     fetch('/api/v1/patients/1').then(res => {
            //         console.log(res);
            //         return res.json();
            //     }).then(resJson => {
            //         console.log(resJson);
            //         self.test = resJson;
            //     });
            // }

            created: function() {
                let self = this
                axios.get('/api/v1/patients/1')
                    .then(function (response) {
                        console.log(response);
                        self.patient = {...response.data};


                    })
                    .catch(function (error) {
                        console.log('Ошибка! Не могу связаться с API. ' + error);
                        // self.answer = 'Ошибка! Не могу связаться с API. ' + error
                    })

                // let self = this;
                // fetch('/api/v1/patients/1').then(res => {
                //     console.log(res);
                //     return res.json();
                // }).then(resJson => {
                //     console.log(resJson);
                //     self.test = resJson;
                // });
            }
        })


    </script>
</c:if>

<c:if test="${pageContext.request.remoteUser == 'nurse'}">
    <div id="app-3" v-if="show">

      <span v-bind:title="message">
        Время загрузки страницы
      </span>

        <input type="checkbox" id="checkbox" v-model="isEdit">
        <label for="checkbox">Edit</label>

        <div style="background: #7abaff; padding: 7px 0;">Patient: {{patient.id}} {{ patient.fullName }}</div>
        <hr/>

        <div v-if="!isEdit">
            <div v-for="(value, name, index) in patient">
                {{ index }}. {{ name }}: {{ value }}
            </div>
        </div>
        <div v-else>

            <input v-model="patient.fullName" placeholder="Full Name">
            <input v-model="patient.address" placeholder="address">
            <input v-model="patient.birthDate" placeholder="birthDate">
            <input v-model="patient.phones" placeholder="phones">
            <input v-model="patient.receiptDate" placeholder="receiptDate">
            <input v-model="patient.complaints" placeholder="complaints">
            <input v-model="patient.diagnosis" placeholder="diagnosis">
            <input v-model="patient.housing" placeholder="housing">
            <input v-model="patient.floor" placeholder="floor">
            <input v-model="patient.ward" placeholder="ward">
            <input v-model="patient.email" placeholder="email">
            <input v-model="patient.nurse" placeholder="nurse">
            <input v-model="patient.cardStatus.value" placeholder="cardStatus">
            <input v-model="patient.cardStatus.text" placeholder="cardStatusText">

            <hr/>
            <button v-on:click="save()">Save</button>

                <%--        <button v-on:click="say('hi')">Скажи hi</button>--%>
                <%--        <button v-on:click="say('what')">Скажи what</button>--%>
        </div>
    </div>


    <script>
        var app3 = new Vue({
            el: '#app-3',
            data: {
                message: 'Вы загрузили эту страницу: ' + new Date().toLocaleString(),
                show: true,
                isEdit: true,
                patient: {
                    "id": "",
                    "userId": "",
                    "address": "",
                    "birthDate": "",
                    "fullName": "",
                    "phones": "",
                    "receiptDate": "",
                    "complaints": "",
                    "diagnosis": "",
                    "housing": "",
                    "floor": "",
                    "ward": "",
                    "email": "",
                    "nurse": "",
                    "cardStatus": "",
                    "cardStatusText": {
                        "value": "",
                        "text": ""
                    }
                }

            },
            created: function() {
                let self = this
                axios.get('/api/v1/patients/5')
                    .then(function (response) {
                        console.log(response);
                        self.patient = {...response.data};


                    })
                    .catch(function (error) {
                        console.log('Ошибка! Не могу связаться с API. ' + error);
                        // self.answer = 'Ошибка! Не могу связаться с API. ' + error
                    })

                // let self = this;
                // fetch('/api/v1/patients/1').then(res => {
                //     console.log(res);
                //     return res.json();
                // }).then(resJson => {
                //     console.log(resJson);
                //     self.test = resJson;
                // });
            },
            methods: {
                getPatientId: function (id) {
                    let self = this;
                    axios.get('/api/v1/patients/' + id)
                        .then(function (response) {
                            console.log(response);
                            self.patient = {...response.data};


                        })
                        .catch(function (error) {
                            console.log('Ошибка! Не могу связаться с API. ' + error);
                            // self.answer = 'Ошибка! Не могу связаться с API. ' + error
                        })
                },
                say: function (message) {
                    alert(message)
                },
                save: function () {
                    let self = this;
                    axios.put('/api/v1/patients/', this.patient)
                        .then(function (response) {
                            console.log(response);
                            // self.patient = {...response.data};

                        })
                        .catch(function (error) {
                            console.log(error);
                            console.log('Ошибка! Не могу связаться с API. ' + error);
                            if (window["showError"]) {
                                window.showError.errorText = error.data;
                                window.showError.show = true;
                            }
                            // self.answer = 'Ошибка! Не могу связаться с API. ' + error
                        })
                },

            }
        })

    </script>
</c:if>


<div id="show-error" v-if="show">

    <%--      <span v-bind:title="message">--%>
    <%--        Время загрузки страницы--%>
    <%--      </span>--%>
    <div style="position: fixed;
     left: 50%;
     top: 20%;
     pointer-events: auto;
     box-sizing: border-box;
     z-index: 1000;
     display: block;
     max-width: 100%;
     max-height: 100%;">
        <div class="alert alert-danger" style="position: relative; left: -50%; border: red 1px solid;">
            <button type="button" style="padding-left: 7px;" class="close" data-dismiss="alert" v-on:click="show=false">
                &times;
            </button>
            <h3 style="border-bottom: red 1px dotted;">Error!</h3>
            Text error message:
            <strong>{{errorText}}</strong>
        </div>
    </div>


</div>


<script>
    var showError = new Vue({
        el: '#show-error',
        data: {
            message: 'Вы загрузили эту страницу: ' + new Date().toLocaleString(),
            show: false,
            errorText: ""
        },
        created: function() {
            this.errorText = "Load page";

        },
        methods: {
            say: function (message) {
                alert(message)
            }
        }
    })

</script>

<!-- !!! part componets end  !!!  -->
