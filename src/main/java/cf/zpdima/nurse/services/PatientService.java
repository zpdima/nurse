package cf.zpdima.nurse.services;

import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.nurse.dto.PatientDTO;

import java.nio.file.AccessDeniedException;
import java.util.List;

public interface PatientService {

    public Patient findById(Long id);

    public List<Patient> getPatients();

    public List<Patient> getByNursePatients(String nurse);

    public int insert(Patient patient);

    public int update(PatientDTO patientDTO);

    public int update(Patient patient);

    public int delete(Patient patient);

    public int delete(Long id, String username) throws AccessDeniedException;

    public int delete(Long id);

}
