package cf.zpdima.nurse.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Component
@WebFilter(urlPatterns = {"/*"},
        initParams = { @WebInitParam(name = "excludedExt", value = "css js svg jpeg jpg png pdf") })
public class LogFilter implements Filter {
    private static final Logger logger = LoggerFactory.getLogger(LogFilter.class);

    //    private FilterConfig config = null;
    private boolean active = true;

    private static Set<String> excluded;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        // TODO Auto-generated method stub
//		loggerger.info(" --- doFilter " + servletRequest.getParameterMap());

        HttpServletRequest httpRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpResponse = (HttpServletResponse) servletResponse;


        if (isExcluded(httpRequest)) {
            filterChain.doFilter(httpRequest, httpResponse);
            return;
        }

        if (active) {

            // Здесь можно вставить код для обработки
            logger.info("--- doFilter remote addr = " + servletRequest.getRemoteAddr() + " user : " + httpRequest.getRemoteUser() + " path : " + httpRequest.getServletPath() + " method: " + httpRequest.getMethod() + " CharacterEncoding = "
                    + servletRequest.getCharacterEncoding() + " locale: " + servletRequest.getLocale());
            Map<String, String[]> map = servletRequest.getParameterMap();
            for (String key : map.keySet()) {
                String value = "";
                for (String c : map.get(key)) {
                    value += c;
                }
                logger.info(" ---  Key : " + key + " Value : " + value);
            }
//            if ("POST".equalsIgnoreCase(httpRequest.getMethod())) {
//                String body = httpRequest.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
//                logger.info(" --- body : '{}'", body);
//
//            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // TODO Auto-generated method stub
        logger.info("--- init logFilter");
        logger.info("--- init logFilter dir : {}", System.getProperty("user.dir"));
//        try {
//            System.out.println("Current dir : " + new java.io.File(".").getCanonicalPath());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        System.out.println("Current user.dir:" + System.getProperty("user.dir"));
//        System.out.println("Current user.dir:" + System.getProperty("user.name"));


//        this.config = filterConfig;
//        String act = config.getInitParameter("active");
//        if (act != null)
//            active = (act.toUpperCase().equals("TRUE"));

//        String excludedString = filterConfig.getInitParameter("excludedExt");
        String excludedString = "css js svg jpeg jpg png pdf";
        logger.info(" --- --- init logFilter excludedExt : {}", excludedString);
        if (excludedString != null) {
            excluded = Collections.unmodifiableSet(
                    new HashSet<>(Arrays.asList(excludedString.split(" ", 0))));
        } else {
            excluded = Collections.<String>emptySet();
        }
        logger.info(" --- --- init logFilter excluded : {}", excluded);

    }

    boolean isExcluded(HttpServletRequest request) {
        String path = request.getRequestURI();
//        logger.info(" --- url {}", path);
        String extension = path.substring(path.indexOf('.', path.lastIndexOf('/')) + 1).toLowerCase();
//        String extension = path.substring(path.length()-3).toLowerCase();
//        logger.info(" --- extension {}", extension);
        return excluded.contains(extension);
    }

}
