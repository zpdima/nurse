package cf.zpdima.nurse.converters;

import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.nurse.dto.PatientDTO;
import cf.zpdima.nurse.repository.I18nRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.converter.Converter;


@Slf4j
public class PatienToDTOConverter implements Converter<Patient, PatientDTO> {

    @Autowired
    I18nRepository i18nRepository;

    public I18nRepository getI18nRepository() {
        return i18nRepository;
    }

    public void setI18nRepository(I18nRepository i18nRepository) {
        this.i18nRepository = i18nRepository;
    }

    @Override
    public PatientDTO convert(Patient patient) {
        log.info(" --- I18nRepository {}", i18nRepository);
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setId(patient.getId());
        patientDTO.setAddress(patient.getAddress());
        patientDTO.setBirthDate(new java.sql.Date(patient.getBirthDate().getTime()));
        patientDTO.setReceiptDate(new java.sql.Date(patient.getReceiptDate().getTime()));
        patientDTO.setComplaints(patient.getComplaints());
        patientDTO.setDiagnosis(patient.getDiagnosis());
        patientDTO.setEmail(patient.getEmail());
        patientDTO.setFloor(patient.getFloor());
        patientDTO.setFullName(patient.getFullName());
        patientDTO.setHousing(patient.getHousing());
        patientDTO.setWard(patient.getWard());
        patientDTO.setPhones(patient.getPhones());
        patientDTO.setNurse(patient.getNurse());
        patientDTO.setCardStatus(patient.getCardStatus());
//        patientDTO.setCardStatusText(conversionService.convert(patient.getCardStatus(), String.class));

        return patientDTO;
    }
}
