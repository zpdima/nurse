<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.3/examples/sign-in/">
    <!-- Custom styles for this template -->
    <link href="https://getbootstrap.com/docs/4.3/examples/sign-in/signin.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">

</head>
<body class="text-center">


<%--<p th:if="${loginError}" class="error">Wrong user or password</p>--%>
<form class="form-signin" action="/login" method="post">

    <c:if test="${loginError}">
        <h3 class="alert alert-danger">
            Error
        </h3>
    </c:if>


    <!--    <img class="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">-->
    <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="form.login.head"/></h1>
    <label for="username" class="sr-only"><fmt:message key="form.login.username"/></label>
    <input type="text" id="username" class="form-control" placeholder="User Name" autofocus name="username">
    <label for="password" class="sr-only"><fmt:message key="form.login.password"/></label>
    <input type="password" id="password" class="form-control" placeholder="Password" name="password">
    <!--    <div class="checkbox mb-3">-->
    <!--        <label>-->
    <!--            <input type="checkbox" value="remember-me"> Remember me-->
    <!--        </label>-->
    <!--    </div>-->
    <button class="btn btn-lg btn-primary btn-block" type="submit"><fmt:message key="form.login.login"/></button>
    <p class="mt-5 mb-3 text-muted">&copy; 2019</p>
</form>

</body>
</html>