package cf.zpdima.nurse.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatientVitalSigns {
    private Long id;
    private Long userId;
    private Long patientId;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss'Z'")
//    @JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
    private Date inputTime;
    private String name;
    private Double value1;
    private Double value2;
}
