<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div style="padding: 25px;">

<%--    <ul>--%>

<%--        <li><a href="${pageContext.request.contextPath}/">Home</a></li>--%>
<%--        <li><a href="${pageContext.request.contextPath}/patients">Patients</a></li>--%>
<%--        <li><a href="${pageContext.request.contextPath}/contactus">Contact Us</a></li>--%>
<%--    </ul>--%>

    <div class="btn-group-vertical" style="width: 100%;">
        <a href="${pageContext.request.contextPath}/"
           class="btn btn-outline-success my-2 my-sm-0"><fmt:message key="menu.home"/></a>
        <a href="${pageContext.request.contextPath}/patients"
           class="btn btn-outline-success my-2 my-sm-0"><fmt:message key="menu.patients"/></a>
        <a href="${pageContext.request.contextPath}/contactus"
           class="btn btn-outline-success my-2 my-sm-0"><fmt:message key="menu.contacts"/></a>
        <a href="${pageContext.request.contextPath}/table"
           class="btn btn-outline-success my-2 my-sm-0"><fmt:message key="menu.table"/></a>

    </div>

</div>

