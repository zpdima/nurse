package cf.zpdima.nurse;

import cf.zpdima.nurse.controllers.ApiController;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

//@WebMvcTest(controllers = ApiController.class)
//@AutoConfigureMockMvc
public class ApiControllerTest {

    private final Logger logger = LoggerFactory.getLogger(ApiControllerTest.class);

    @Autowired
    MockMvc mockMvc;

//    @Test
    public void addTest() throws Exception {

//        logger.info(mockMvc.toString());
        logger.info(" ---- RUN TEST");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/patients")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "        \"address\": \"Донецкая 33\",\n" +
                        "        \"birthDate\": \"1972-05-25\",\n" +
                        "        \"fullName\": \"Орехов А.У.\",\n" +
                        "        \"phones\": \"(099) 999-14-99\",\n" +
                        "        \"receiptDate\": \"2019-12-11\",\n" +
                        "        \"complaints\": \"Боли в животе\",\n" +
                        "        \"diagnosis\": \"Язва\",\n" +
                        "        \"housing\": 22,\n" +
                        "        \"floor\": null,\n" +
                        "        \"ward\": 41,\n" +
                        "        \"email\": null,\n" +
                        "        \"secret\": null\n" +
                        "    }"))
                .andExpect(MockMvcResultMatchers.status().isOk());


    }

}
