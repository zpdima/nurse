update patients
set card_status = 'NEW';

ALTER TABLE springjooq.patients
MODIFY COLUMN card_status varchar(255) NOT NULL DEFAULT 'NEW' AFTER secret;
