package cf.zpdima.nurse.config;


import cf.zpdima.nurse.converters.DTOToPatienConverter;
import cf.zpdima.nurse.converters.PatienToDTOConverter;
import cf.zpdima.nurse.formatters.CardPatientStatusFormatter;
import cf.zpdima.nurse.repository.I18nRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
@EnableScheduling
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    I18nRepository i18nRepository;

    private final Logger logger = LoggerFactory.getLogger(WebConfig.class);

    @Override
    public void addFormatters(FormatterRegistry registry) {

        logger.info(" --- addFormatters(cardPatientStatusFormatter)");
        logger.info(" --- i18nRepository {}", i18nRepository);
        CardPatientStatusFormatter cardPatientStatusFormatter = new CardPatientStatusFormatter();
        cardPatientStatusFormatter.setI18nRepository(i18nRepository);
        registry.addFormatter(cardPatientStatusFormatter);


        logger.info(" --- addFormatters");
        registry.addConverter(new DTOToPatienConverter());
        PatienToDTOConverter patienToDTOConverter = new PatienToDTOConverter();
//        patienToDTOConverter.setI18nRepository(i18nRepository);
        registry.addConverter(patienToDTOConverter);

    }
}
