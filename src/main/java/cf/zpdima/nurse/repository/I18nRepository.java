package cf.zpdima.nurse.repository;

import cf.zpdima.nurse.domain.I18n;
import cf.zpdima.spring.jooq.sample.model.Tables;
import org.jooq.DSLContext;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class I18nRepository {

    private static Logger logger =
            LoggerFactory.getLogger(I18nRepository.class);

    @Autowired
    private Environment environment;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    DSLContext dsl;

    @PostConstruct
    public void init() {
        logger.info(" --- PostConstructor {}");
        for (final String profileName : environment.getActiveProfiles()) {
            logger.info(" --- --- Currently active profile - {}", profileName);
        }
    }

    public I18n findById(Long id) {
        return mapper.map(dsl.selectFrom(Tables.I18N).
                where(Tables.I18N.ID.equal(id)).fetchOne(), I18n.class);
    }

    public I18n findByNameAndKeyAndLang(String name, String key, String lang) {
        return mapper.map(dsl.selectFrom(Tables.I18N).
                where(Tables.I18N.NAME.equal(name).and(Tables.I18N.KEY.equal(key))
                .and(Tables.I18N.LANG.equal(lang))).fetchOne(), I18n.class);
    }

    public I18n findByNameAndLangAndValue(String name, String lang, String value) {
        return mapper.map(dsl.selectFrom(Tables.I18N).
                where(Tables.I18N.NAME.equal(name).and(Tables.I18N.LANG.equal(lang))
                        .and(Tables.I18N.LANG.equal(value))).fetchOne(), I18n.class);
    }

}
