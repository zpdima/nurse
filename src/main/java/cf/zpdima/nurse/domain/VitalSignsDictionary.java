package cf.zpdima.nurse.domain;

import cf.zpdima.nurse.domain.enums.VitalSignsType;
import cf.zpdima.nurse.dto.View;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

@Data
public class VitalSignsDictionary  extends VitalSignsDataScale{

    private Long pk;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private String name;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private String unit;

    private Byte enabled;

    private Integer sortOrder;

    @JsonView({View.VitalsList.class, View.VitalsEdit.class})
    private VitalSignsType type;

}
