package cf.zpdima.nurse.utils;

import cf.zpdima.nurse.domain.enums.CardPatientStatus;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;

import java.io.IOException;

public class CardPatientStatusSerializer extends StdSerializer<CardPatientStatus> {

    @Autowired
    ConversionService conversionService;


//    @Override
//    public void serialize(CardPatientStatus cardPatientStatus, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
//
//
//    }

    public CardPatientStatusSerializer() {
        this(null);
    }

    public CardPatientStatusSerializer(Class<CardPatientStatus> t) {
        super(t);
    }


    @Override
    public void serialize(
            CardPatientStatus value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeString(conversionService.convert(value, String.class));

//        jgen.writeStartObject();
//        jgen.writeStringField("value", value.toString());
//        jgen.writeStringField("text", conversionService.convert(value, String.class));
////        CardPatientStatus cardPatientStatus = value;
////        jgen.writeString(value.toString());
//        jgen.writeEndObject();
    }
}
