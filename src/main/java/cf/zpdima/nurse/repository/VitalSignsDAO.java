package cf.zpdima.nurse.repository;

import cf.zpdima.nurse.domain.PatientVitalSigns;
import cf.zpdima.nurse.domain.VitalSignsData;
import cf.zpdima.nurse.domain.VitalSignsDataScale;
import cf.zpdima.nurse.domain.VitalSignsDictionary;
import cf.zpdima.spring.jooq.sample.model.tables.records.VitalSignsDataRecord;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import static org.jooq.impl.DSL.select;


import static cf.zpdima.spring.jooq.sample.model.Tables.*;

@Service
@Slf4j
public class VitalSignsDAO {

    @Autowired
    DSLContext dsl;

    public List<VitalSignsData> getPatientVitalSigns() {
        return dsl.selectFrom(VITAL_SIGNS_DATA)
                .fetch().into(VitalSignsData.class);

    }

    public VitalSignsDictionary findVitalSignsDictionaryByName(String name) {
        VitalSignsDictionary vitalSignsDictionary = dsl.selectFrom(VITAL_SIGNS_DICTIONARY).
                where(VITAL_SIGNS_DICTIONARY.NAME.equal(name)).fetchOneInto(VitalSignsDictionary.class);
        return vitalSignsDictionary;
    }

    //    https://www.jooq.org/doc/3.12/manual/sql-execution/fetching/pojos/
    public Long insert(VitalSignsData vitalSignsData) {
        int r = 0;
        VitalSignsDataRecord vitalSignsDataRecord = dsl.newRecord(VITAL_SIGNS_DATA, vitalSignsData);
        r = vitalSignsDataRecord.store();
        Long id = vitalSignsDataRecord.getPk();
        return id;
    }



    public Long  save(VitalSignsData vitalSignsData){
        int r = 0;
        VitalSignsDataRecord vitalSignsDataRecord = dsl.newRecord(VITAL_SIGNS_DATA, vitalSignsData);
        if(vitalSignsDataRecord.getPk() == null) {
            r = vitalSignsDataRecord.store();
        }else{
            r = vitalSignsDataRecord.update();
        }
        Long id =  vitalSignsDataRecord.getPk();
        return id;
    }

    //https://stackoverflow.com/questions/23940701/jooq-nested-query
    public List<VitalSignsData> findVitalSignsDataAndStyleByPatientFkAndIndicatorTime(Long patientPk, LocalDateTime dateTimeBegin, LocalDateTime dateTimeEnd) {

        VitalSignsDataScale vitalSignsDataScale;

        List<VitalSignsData> list = dsl.select(VITAL_SIGNS_DATA.fields())
                .select(VITAL_SIGNS_DICTIONARY.NAME, VITAL_SIGNS_DICTIONARY.TYPE, VITAL_SIGNS_DICTIONARY.UNIT)
                .select(select(VITAL_SIGNS_DATA_SCALE.STYLE_CLASS)
                        .from(VITAL_SIGNS_DATA_SCALE)
                        .where(VITAL_SIGNS_DATA_SCALE.VITAL_SIGNS_DICTIONARY_FK.eq(VITAL_SIGNS_DATA.VITAL_SIGNS_DICTIONARY_FK))
                        .and(VITAL_SIGNS_DATA.VALUE1.between(VITAL_SIGNS_DATA_SCALE.CRITICAL_VALUE1_START).and(VITAL_SIGNS_DATA_SCALE.CRITICAL_VALUE1_END)
                                .or(VITAL_SIGNS_DATA.VALUE2.between(VITAL_SIGNS_DATA_SCALE.CRITICAL_VALUE2_START).and(VITAL_SIGNS_DATA_SCALE.CRITICAL_VALUE2_END)))
                        .orderBy(VITAL_SIGNS_DATA_SCALE.PRIORITY.desc())
                        .limit(1)
                        .asField("styleClass")
                )
                .from(VITAL_SIGNS_DATA)
                .leftJoin(VITAL_SIGNS_DICTIONARY).on(VITAL_SIGNS_DICTIONARY.PK.eq(VITAL_SIGNS_DATA.VITAL_SIGNS_DICTIONARY_FK))
                .where(VITAL_SIGNS_DATA.PATIENT_FK.equal(patientPk))
                .and(VITAL_SIGNS_DATA.MODIFIED_TIME.between(Timestamp.valueOf(dateTimeBegin)).and(Timestamp.valueOf(dateTimeEnd)))
                .orderBy(VITAL_SIGNS_DATA.INDICATOR_TIME.desc())
                .fetch()
                .into(VitalSignsData.class);
        return list;
    }

    public List<VitalSignsDictionary> findVitalSignsDictionaryByEnabled(Byte enabled) {
        return dsl.select()
                .from(VITAL_SIGNS_DICTIONARY)
                .where(VITAL_SIGNS_DICTIONARY.ENABLED.equal(enabled))
                .orderBy(VITAL_SIGNS_DICTIONARY.SORT_ORDER.asc())
                .fetch()
                .into(VitalSignsData.class);
    }


}
