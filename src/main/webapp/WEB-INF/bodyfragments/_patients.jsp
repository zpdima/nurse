<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<h2><fmt:message key="menu.patients"/></h2>
<a href="add-patient"
   class="btn btn-outline-success my-2 my-sm-0"><fmt:message key="menu.add"/></a>
<table class="table table-striped">
    <thead>
    <tr>
        <th><fmt:message key="patient.id"/></th>
        <th><fmt:message key="patient.receiptDate"/></th>
        <th><fmt:message key="patient.fullName"/></th>
        <th><fmt:message key="patient.address"/></th>
        <th><fmt:message key="patient.birthDate"/></th>
<%--        <sec:authorize access="hasRole('ROLE_NURSEADMIN')">--%>
            <th>Action</th>
<%--        </sec:authorize>--%>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="record" items="${patients}">
        <tr>
            <td>${record.id}</td>
            <td><fmt:formatDate value="${record.receiptDate}" pattern="dd.MM.yyyy"/></td>
            <td>${record.fullName}</td>
            <td>${record.address}</td>
            <td><fmt:formatDate value="${record.birthDate}" pattern="dd.MM.yyyy"/></td>
<%--            <sec:authorize access="hasRole('ROLE_NURSEADMIN')">--%>
                <td><a href="edit-patient?id=${record.id}"
                       class="btn btn-outline-success my-2 my-sm-0"><fmt:message key="patient.edit"/></a>
                    <a class="btn btn-outline-danger my-2 my-sm-0"
                       onclick="getConfirmationDelete(${record.id}, '${record.fullName}')">X</a>
                </td>
<%--            </sec:authorize>--%>

<%--            <sec:authorize access="hasRole('ROLE_NURSE')">--%>
<%--                <td><a href="edit-patient?id=${record.id}"--%>
<%--                       class="btn btn-success custom-width">View</a>--%>
<%--                </td>--%>
<%--            </sec:authorize>--%>
        </tr>
    </c:forEach>
    </tbody>
</table>

</div>
<div style="color: red;" id="feedback"></div>


<div class="container" id="feedback-show" style="color: red; display: none;">

</div>



<script type="text/javascript">
    function getConfirmationDelete(id, fullName) {
        var retVal = confirm("Delete " + fullName + "?");
        if (retVal == true) {
            // window.location.href = '/patients/patients/' + id;
            deletePatient(id);
            return true;
        } else {
            return false;
        }
    }

    function deletePatient(id) {
        console.log(" --- delete id = " + id);
        $.ajax({
            type: "DELETE",
            url: "/api/v1/patients/" + id,
            data: "name=someValue",
            success: function (msg) {
                // alert("Data Deleted: " + msg);
                location.reload();
            },
            error: function (e) {
                console.log(e);
                console.log("ERROR : ", e);
                var errorText = "<h4>Error delete : </h4><ul>";
                errorText += "<li>" + e.status + "</li>";
                if(e.responseJSON.errors !== undefined) {
                    for (var i in e.responseJSON.errors) {
                        errorText += "<li><strong>field: </strong>" + e.responseJSON.errors[i].field
                            + " <strong>message: </strong>" + e.responseJSON.errors[i].defaultMessage
                            + " <strong>value: </strong>" + e.responseJSON.errors[i].rejectedValue
                            + "</li>\n"
                    }
                }else{
                    errorText += "<li>" + e.responseJSON.message + "</li>";
                }

                errorText += "</ul>";
                $('#feedback').html(errorText);
                $('#feedback-show').html(errorText);
                $('#feedback-show').show(errorText);
                alert("Ошибка удаления");
            }
        });
    }
</script>

