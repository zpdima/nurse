package cf.zpdima.nurse.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public static boolean serialisation(String namefile, Object o) {

        FileOutputStream fos;
        try {
            fos = new FileOutputStream(namefile);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(o);
            oos.flush();
            oos.close();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return true;

    }

    public static Object deserilization(String namefile) {

        FileInputStream fis;
        Object o = null;
        try {
            fis = new FileInputStream(namefile);
            ObjectInputStream oin = new ObjectInputStream(fis);
            o = oin.readObject();

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return o;
    }

    public static String now() {
        return (new SimpleDateFormat("dd.MM.yy HH:mm:ss", new Locale("RU"))).format(new Date());
    }

    public static String strDate(Date date) {
        return (new SimpleDateFormat("dd.MM.yy HH:mm:ss", new Locale("RU"))).format(date);
    }

}
