package cf.zpdima.nurse;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiControllerTomcatTest {

    private final Logger logger = LoggerFactory.getLogger(ApiControllerTomcatTest.class);

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(username = "nurse", roles={"NURSE"})
    public void addTestBad() throws Exception {
        logger.info(" ---- RUN TEST BAD");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/patients")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "        \"address\": \"Донецкая 33\",\n" +
                        "        \"birthDate\": \"1972-05-25\",\n" +
                        "        \"fullName\": \"Орехов А.У.\",\n" +
                        "        \"phones\": \"(099) 999-14-99aa\",\n" +
                        "        \"receiptDate\": \"2019-21-11\",\n" +
                        "        \"complaints\": \"Боли в животе\",\n" +
                        "        \"diagnosis\": \"Язва\",\n" +
                        "        \"housing\": 22,\n" +
                        "        \"floor\": null,\n" +
                        "        \"ward\": 41,\n" +
                        "        \"email\": null,\n" +
                        "        \"secret\": null,\n" +
                        "        \"nurse\": \"nurse3\",\n" +
                        "        \"cardStatus\": \"NEW\"\n" +
                        "    }"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    @WithMockUser(username = "nurse", roles={"NURSE"})
    public void addTestOk() throws Exception {
        logger.info(" ---- RUN TEST Ok");

        mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/patients")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n" +
                        "        \"address\": \"Донецкая 33\",\n" +
                        "        \"birthDate\": \"1972-05-25\",\n" +
                        "        \"fullName\": \"Орехов А.У.\",\n" +
                        "        \"phones\": \"(099) 999-14-99\",\n" +
                        "        \"receiptDate\": \"2019-11-11\",\n" +
                        "        \"complaints\": \"Боли в животе\",\n" +
                        "        \"diagnosis\": \"Язва\",\n" +
                        "        \"housing\": 22,\n" +
                        "        \"floor\": null,\n" +
                        "        \"ward\": 41,\n" +
                        "        \"email\": null,\n" +
                        "        \"secret\": null,\n" +
                        "        \"nurse\": \"nurse3\",\n" +
                        "        \"cardStatus\": \"NEW\"\n" +
                        "    }"))
                .andExpect(MockMvcResultMatchers.status().isOk());

    }

}
