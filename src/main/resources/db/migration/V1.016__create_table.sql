CREATE TABLE `patient_vital_signs` (
                            `id`  bigint NOT NULL AUTO_INCREMENT ,
                            `patient_id`  bigint NULL ,
                            `user_id`  bigint NULL ,
                            `input_time`  timestamp not NULL,
                            `name`  varchar(60) NULL ,
                            `value1`  decimal(10,4) NULL ,
                            `value2`  decimal(10,4) NULL ,
                            PRIMARY KEY (`id`)
);
