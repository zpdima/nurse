package cf.zpdima.nurse.dto;

import cf.zpdima.nurse.domain.enums.CardPatientStatus;
import cf.zpdima.nurse.utils.CardPatientStatusSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientDTO {

    private Long id;

    private Long userId;

    private String address;

    @NotNull
    private Date birthDate;

    @NotBlank
    @Size(min=5, max=60)
    private String fullName;

    @Pattern(regexp = "\\([0-9]{3}\\) \\d\\d\\d-\\d\\d-\\d\\d", message = "format phone: (123) 456-78-90")
    private String phones;

    @Past
    @NotNull
    private Date receiptDate;

    @NotBlank
    private String complaints;

    private String diagnosis;

    private Long housing;

    private Long floor;

    private Long ward;

    @Email
    private String email;

    private String nurse;

    @NotNull
    @JsonSerialize(using = CardPatientStatusSerializer.class)
    private CardPatientStatus cardStatus;

    private String cardStatusText;

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", birthDate=" + birthDate +
                ", fullName='" + fullName + '\'' +
                ", phones='" + phones + '\'' +
                ", email='" + email + '\'' +
                ", receiptDate=" + receiptDate +
                ", complaints='" + complaints + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", housing=" + housing +
                ", floor=" + floor +
                ", ward=" + ward +
                ", nurse=" + nurse +
                ", cardStatus=" + cardStatus +
                ", cardStatusText=" + cardStatusText +
                '}';
    }

}
