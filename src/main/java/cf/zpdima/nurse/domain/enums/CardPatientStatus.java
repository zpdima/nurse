package cf.zpdima.nurse.domain.enums;

public enum CardPatientStatus {
    NEW, RECEPTION, HOSPITAL, DISCHARGED;
}
