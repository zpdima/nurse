package cf.zpdima.nurse.services;

import java.util.List;
import java.util.stream.Collectors;

import cf.zpdima.nurse.domain.Book;
import cf.zpdima.nurse.domain.Patient;
import cf.zpdima.nurse.repository.PatientRepository;
import org.jooq.DSLContext;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cf.zpdima.spring.jooq.sample.model.Tables;


@Service
public class BookService {

	@Autowired
	private ModelMapper mapper;
			
	@Autowired
	DSLContext dsl;



	public List<Book> getBooks() {
		return dsl
				.selectFrom(Tables.BOOKS)
				.fetch()
				.stream()
				.map(e -> mapper.map(e, Book.class))
				.collect(Collectors.toList());
	}


}
