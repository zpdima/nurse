CREATE TABLE i18n (
                        id  bigint(20) NOT NULL auto_increment,
                        name  varchar(512) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
                        `key`  varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
                        lang  varchar(3) NOT NULL,
                        value  varchar(255) NOT NULL,
                        PRIMARY KEY (id),
                        UNIQUE INDEX i18n_nkv_uk (name, `key`, lang)
);


INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'NEW', 'ru', 'новый');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'RECEPTION', 'ru', 'прием');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'HOSPITAL', 'ru', 'стационар');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'DISCHARGED', 'ru', 'выписан');

INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'NEW', 'en', 'New');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'RECEPTION', 'en', 'Reception');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'HOSPITAL', 'en', 'Hospital');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'DISCHARGED', 'en', 'Discharged');

INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'NEW', 'de', 'Neu');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'RECEPTION', 'de', 'Rezeption');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'HOSPITAL', 'de', 'Krankenhaus');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'DISCHARGED', 'de', 'Entladen');

INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'NEW', 'fr', 'Nouveau');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'RECEPTION', 'fr', 'accueil');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'HOSPITAL', 'fr', 'Hôpital');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'DISCHARGED', 'fr', 'Déchargé');

INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'NEW', 'pt', 'Novo');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'RECEPTION', 'pt', 'Recepção');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'HOSPITAL', 'pt', 'Hospital');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'DISCHARGED', 'pt', 'Descarregado');

INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'NEW', 'pl', 'Nowy');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'RECEPTION', 'pl', 'Recepcja');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'HOSPITAL', 'pl', 'Szpital');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'DISCHARGED', 'pl', 'Rozładowany');
