package cf.zpdima.nurse.domain;


import lombok.Data;

@Data
public class User {
    private String username;
}
