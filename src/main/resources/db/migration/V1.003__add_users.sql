INSERT INTO springjooq.users(username, password, enabled) VALUES ('nurse', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);
INSERT INTO springjooq.users(username, password, enabled) VALUES ('nurseadmin', '{bcrypt}$2a$10$cyf5NfobcruKQ8XGjUJkEegr9ZWFqaea6vjpXWEaSqTa2xL9wjgQC', 1);

INSERT INTO springjooq.authorities(username, authority) VALUES ('nurse', 'ROLE_NURSE');
INSERT INTO springjooq.authorities(username, authority) VALUES ('nurseadmin', 'ROLE_NURSEADMIN');

