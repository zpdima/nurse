DROP TABLE IF EXISTS vital_signs_data_scale;
DROP TABLE IF EXISTS vital_signs_data;
DROP TABLE IF EXISTS vital_signs_dictionary;

CREATE TABLE vital_signs_dictionary  (
                                         pk bigint NOT NULL AUTO_INCREMENT,
                                         name varchar(120) NOT NULL,
                                         unit varchar(30) NOT NULL,
                                         type enum('DECIMAL', 'OBJECT') NOT NULL,
                                         enabled tinyint NULL DEFAULT 1,
                                         sort_order int NULL,
                                         createdTS TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
                                         modifiedTS TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Modified',
                                         created_by INT NULL COMMENT 'Created By',
                                         modified_by INT NULL COMMENT 'Modified By',
                                         PRIMARY KEY (pk)
);

CREATE TABLE vital_signs_data  (
                                   pk bigint NOT NULL AUTO_INCREMENT,
                                   vital_signs_dictionary_fk bigint NOT NULL,
                                   patient_fk bigint NOT NULL,
                                   source enum('MANUAL','SENSOR') NOT NULL,
                                   measuring varchar(255) NULL,
                                   notes varchar(255) NULL,
                                   user_fk int null,
                                   indicator_time  timestamp not NULL DEFAULT CURRENT_TIMESTAMP,
                                   modified_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                   value1 decimal(10, 4) NULL,
                                   value2 decimal(10, 4) NULL,
                                   createdTS TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Created',
                                   modifiedTS TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Modified',
                                   created_by INT NULL COMMENT 'Created By',
                                   modified_by INT NULL COMMENT 'Modified By',
                                   PRIMARY KEY (pk)
);

ALTER TABLE vital_signs_data
    ADD CONSTRAINT fk_vital_signs_data_dictionary FOREIGN KEY (vital_signs_dictionary_fk) REFERENCES vital_signs_dictionary (pk),
            ADD CONSTRAINT fk_vital_signs_data_patient FOREIGN KEY (patient_fk) REFERENCES patients (id);

CREATE TABLE vital_signs_data_scale  (
                                         pk bigint NOT NULL AUTO_INCREMENT,
                                         vital_signs_dictionary_fk bigint NOT NULL,
                                         notification_fk int NULL,
                                         style_class varchar(70) NULL,
                                         priority tinyint NOT NULL DEFAULT 1,
                                         critical_value1_start decimal(10, 4) NULL,
                                         critical_value1_end decimal(10, 4) NULL,
                                         critical_value2_start decimal(10, 4) NULL,
                                         critical_value2_end decimal(10, 4) NULL,
                                         PRIMARY KEY (pk)
);

-- ALTER TABLE vital_signs_data_scale
--     ADD CONSTRAINT fk_vital_signs_data_scale_notification FOREIGN KEY (notification_fk) REFERENCES notifications (PK);


INSERT INTO vital_signs_dictionary(name, unit, type) VALUES ('blood_pressure', 'mm Hg', 'object');
SELECT @dictionaryId := LAST_INSERT_ID();
INSERT INTO vital_signs_data(vital_signs_dictionary_fk, patient_fk, source, user_fk, indicator_time, value1, value2)  VALUES (@dictionaryId, 1, 'MANUAL', 4, '2020-01-03 13:57:26', 120.0000, 80.0000);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-danger white', 20, 0.0000, 89.9999, 0.0000, 59.9999);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-success white', 5, 90.0000, 120.0000, 60.0000, 80.0000);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-warning white', 10, 120.9999, 140.0000, 80.9999, 100.0000);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-danger white', 20, 140.9999, 10000.0000, 100.9999, 10000.0000);

INSERT INTO vital_signs_dictionary(name, unit, type) VALUES ('pulse', 'beats per minute', 'decimal');
SELECT @dictionaryId := LAST_INSERT_ID();
INSERT INTO vital_signs_data(vital_signs_dictionary_fk, patient_fk, source, user_fk, indicator_time, value1, value2) VALUES (@dictionaryId, 1, 'MANUAL', 4, '2020-01-03 13:57:53', 70.0000, NULL);

INSERT INTO vital_signs_dictionary(name, unit, type) VALUES ('temperature', '°C', 'decimal');
SELECT @dictionaryId := LAST_INSERT_ID();
INSERT INTO vital_signs_data(vital_signs_dictionary_fk, patient_fk, source, user_fk, indicator_time, value1, value2) VALUES (@dictionaryId, 1, 'MANUAL', 4, '2020-01-03 13:58:13', 36.6000, NULL);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-danger white', 20, 0.0000, 34.9999, NULL, NULL);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-success white', 5, 35.0000, 37.4999, NULL, NULL);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-warning white', 10, 37.5000, 38.9999, NULL, NULL);
INSERT INTO vital_signs_data_scale(vital_signs_dictionary_fk, notification_fk, style_class, priority, critical_value1_start, critical_value1_end, critical_value2_start, critical_value2_end) VALUES (@dictionaryId, NULL, 'bg-danger white', 20, 39.0000, 10000.0000, NULL, NULL);

INSERT INTO vital_signs_dictionary(name, unit, type) VALUES ('weight', 'kg', 'decimal');
SELECT @dictionaryId := LAST_INSERT_ID();
INSERT INTO vital_signs_data(vital_signs_dictionary_fk, patient_fk, source, user_fk, indicator_time, value1, value2) VALUES (@dictionaryId, 1, 'MANUAL', 4, '2020-01-03 13:58:31', 78.2000, NULL);

INSERT INTO vital_signs_dictionary(name, unit, type) VALUES ('height', 'm', 'DECIMAL');
INSERT INTO vital_signs_dictionary(name, unit, type) VALUES ('blood_sugar', 'mg/dl', 'DECIMAL');
INSERT INTO vital_signs_dictionary(name, unit, type) VALUES ('SpO2', '%', 'DECIMAL');
