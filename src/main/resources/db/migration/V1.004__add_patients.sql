-- drop table if exists `patients`;

CREATE TABLE patients (
  id bigint NOT NULL AUTO_INCREMENT,
  address varchar(255) DEFAULT NULL,
  birth_date date DEFAULT NULL,
  full_name varchar(255) DEFAULT NULL,
  phones varchar(255)  DEFAULT NULL,
  receipt_date date DEFAULT NULL,
  complaints varchar(255) DEFAULT NULL,
  diagnosis varchar(255) DEFAULT NULL,
  housing bigint(20),
  floor bigint(20),
  ward bigint(20),
  PRIMARY KEY (id)
);

insert  into patients(id,address,birth_date,complaints,diagnosis,full_name,phones,housing,ward) values
(1,'ул. Иванова 34 кв. 44','1981-05-11','Повышенное давление','-','Геращенко В.П.','(099) 100-33-01',17,38),
(2,'Донецкая 33','1972-05-25','Боли в животе','Язва','Орехов А.У.','(099) 999-14-99',22,41),
(3,'ул. Рабочая 181','1992-07-22','Стал хуже видеть левый глаз','Катаракта','Рахимов А.О.','(093) 414-15-77',25,39),
(4,'пр-т. св.Кирилла и Мефодия 4 кв. 104','1992-10-23','Пропал аппетит','Направление к гастроэнтерологу','Долматов Ф.Е.','(095) 743-58-22',18,45),
(5,'ул. Лавренева 6 кв. 51','1996-05-24','Красное горло, больно глотать','Ангина','Егоров Р.В.','(095) 476-16-44',26,46);

