package cf.zpdima.nurse.domain;

import lombok.Data;

@Data
public class Book {
	private String title;
}
