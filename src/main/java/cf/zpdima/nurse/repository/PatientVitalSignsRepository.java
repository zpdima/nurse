package cf.zpdima.nurse.repository;

import cf.zpdima.nurse.domain.PatientVitalSigns;
import cf.zpdima.spring.jooq.sample.model.Tables;
import cf.zpdima.spring.jooq.sample.model.tables.records.PatientVitalSignsRecord;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static cf.zpdima.spring.jooq.sample.model.Tables.PATIENT_VITAL_SIGNS;

@Service
public class PatientVitalSignsRepository {

    @Autowired
    DSLContext dsl;

    public List<PatientVitalSigns> getPatientVitalSigns() {
        return dsl.selectFrom(PATIENT_VITAL_SIGNS)
                .fetch().into(PatientVitalSigns.class);

    }

//    https://www.codota.com/code/java/methods/org.jooq.DSLContext/insertInto
//    @Override
//    public EXECUTE insert(P pojo, boolean onDuplicateKeyIgnore) {
//        Objects.requireNonNull(pojo);
//        return queryExecutor().execute(dslContext -> {
//            InsertSetMoreStep<R> insertStep = dslContext.insertInto(getTable()).set(newRecord(dslContext, pojo));
//            return onDuplicateKeyIgnore?insertStep.onDuplicateKeyIgnore():insertStep;
//        });
//    }

//    !!!!!!!!!!!!!!!!!!!!!!
//    https://stackoverflow.com/questions/40982328/jooq-mapper-from-pojo-to-record
//    https://www.jooq.org/doc/3.12/manual/sql-execution/fetching/pojos/

    public Long  insert(PatientVitalSigns patientVitalSigns){
        int r = 0;
        PatientVitalSignsRecord patientVitalSignsRecord = dsl.newRecord(PATIENT_VITAL_SIGNS, patientVitalSigns);
        r = patientVitalSignsRecord.store();
        Long id =  patientVitalSignsRecord.getId();
        return id;
    }


    public Long  save(PatientVitalSigns patientVitalSigns){
        int r = 0;
        PatientVitalSignsRecord patientVitalSignsRecord = dsl.newRecord(PATIENT_VITAL_SIGNS, patientVitalSigns);
        if(patientVitalSigns.getId() == null) {
            r = patientVitalSignsRecord.store();
        }else{
            r = patientVitalSignsRecord.update();
        }
        Long id =  patientVitalSignsRecord.getId();
        return id;
    }

}
