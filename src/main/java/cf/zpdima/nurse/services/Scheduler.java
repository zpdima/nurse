package cf.zpdima.nurse.services;

import cf.zpdima.nurse.domain.PatientVitalSigns;
import cf.zpdima.nurse.domain.VitalSignsData;
import cf.zpdima.nurse.domain.VitalSignsDictionary;
import cf.zpdima.nurse.domain.enums.VitalSignsSource;
import cf.zpdima.nurse.repository.PatientVitalSignsRepository;
import cf.zpdima.nurse.repository.VitalSignsDAO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Random;
import java.util.TimeZone;

@Service
@Slf4j
public class Scheduler {

    @Autowired
    PatientVitalSignsRepository patientVitalSignsRepository;

    @Autowired
    VitalSignsDAO vitalSignsDAO;


    @PostConstruct
    public void init() {
        log.info(" --- SchedulerService @PostConstruct");
        log.info(" --- Default System Timezone: " + TimeZone.getDefault().getID());
//        TimeZone.setDefault(TimeZone.getTimeZone("Europe/Kiev"));
        log.info(" --- Using Timezone: " + TimeZone.getDefault().getID());

    }

//    @Scheduled(fixedDelay = 5000)
    public void addNewRecord() {
        log.info("Fixed delay task - " + System.currentTimeMillis() / 1000);
        log.info(" --- Start addNewRecord ");

        PatientVitalSigns patientVitalSigns = new PatientVitalSigns();
        patientVitalSigns.setName("p");
        patientVitalSigns.setPatientId(1l);
        patientVitalSigns.setUserId(1l);
        patientVitalSigns.setValue1(36.6);

        Long id  = patientVitalSignsRepository.insert(patientVitalSigns);

        Random rand = new Random(System.currentTimeMillis());

        try {
            VitalSignsDictionary vitalSignsDictionary = vitalSignsDAO.findVitalSignsDictionaryByName("blood_pressure");
            VitalSignsData vitalSignsData = new VitalSignsData();
            vitalSignsData.setPatientFk(1);
            vitalSignsData.setVitalSignsDictionaryFk(vitalSignsDictionary.getPk());
            vitalSignsData.setSource(VitalSignsSource.SENSOR);
            vitalSignsData.setValue1(rand.nextInt(70) + 100f);
            vitalSignsData.setValue2(rand.nextInt(50) + 70f);
            vitalSignsDAO.insert(vitalSignsData);

            vitalSignsDictionary = vitalSignsDAO.findVitalSignsDictionaryByName("temperature");
            vitalSignsData = new VitalSignsData();
            vitalSignsData.setPatientFk(1);
            vitalSignsData.setVitalSignsDictionaryFk(vitalSignsDictionary.getPk());
            vitalSignsData.setSource(VitalSignsSource.SENSOR);
            vitalSignsData.setValue1(rand.nextFloat()*10 + 30f);
            vitalSignsDAO.insert(vitalSignsData);


        } catch (Exception e) {
            log.error(" --- error:", e);
        }


    }
}
