ALTER TABLE authorities DROP FOREIGN KEY authorities_ibfk_1;

ALTER TABLE users
    ADD COLUMN id  bigint NOT NULL AUTO_INCREMENT FIRST ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (id);

ALTER TABLE authorities
    MODIFY COLUMN username  varchar(25)  NULL ,
ADD COLUMN id  bigint NOT NULL AUTO_INCREMENT FIRST ,
ADD COLUMN user_id  bigint NOT NULL AFTER id,
ADD PRIMARY KEY (id);

update authorities a
set a.user_id = (select id from users u where u.username = a.username);

ALTER TABLE authorities
    DROP INDEX ix_auth_username;

ALTER TABLE authorities
    ADD INDEX authorities_uk (user_id, authority) ;

ALTER TABLE patients
    ADD COLUMN user_id  bigint(20) NULL AFTER id;

update patients p
set p.user_id = (select id from users u where u.username = p.nurse);
