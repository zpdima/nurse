package cf.zpdima.nurse.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private final Logger logger = LoggerFactory.getLogger(WebMvcConfig.class);

    @Bean
    public LocaleChangeInterceptor localeChangeInterceptor() {
        logger.info(" --- localeChangeInterceptor ");
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("lang");
        return lci;
    }

    @Bean
    CookieLocaleResolver localeResolver() {
        logger.info(" --- localeResolver ");
        CookieLocaleResolver clr = new CookieLocaleResolver();
        clr.setCookieName("locale");
        return clr;
    }

//    @Bean
//    public LocaleResolver localeResolver() {
//        SessionLocaleResolver slr = new SessionLocaleResolver();
//        slr.setDefaultLocale(Locale.US);
//        return slr;
//    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // TODO Auto-generated method stub
        logger.info(" --- addInterceptors add  localeChangeInterceptor()");
        registry.addInterceptor(localeChangeInterceptor());
    }
}
