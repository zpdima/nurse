INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'NEW', 'ua', 'новий');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'RECEPTION', 'ua', 'прийом');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'HOSPITAL', 'ua', 'стаціонар');
INSERT INTO i18n (name, `key`, lang, value) VALUES ('cf.zpdima.nurse.domain.enums.CardPatientStatus', 'DISCHARGED', 'ua', 'виписаний');
