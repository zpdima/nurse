<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<h2><fmt:message key="patient.id"/>: ${patient.id}  <fmt:message key="patient.fullName"/>: ${patient.fullName}</h2>
<form action="javascript:console.log( 'submit' );" name="patientForm" id="patientForm" novalidate>
    <div class="form-group">
        <label>ID: ${patient.id}</label>
        <input type="text" id="id" name="id" value="${patient.id}" hidden>
    </div>

    <div class="form-group">
        <label for="fullName"><fmt:message key="patient.fullName"/></label>
        <input type="text" class="form-control" id="fullName" name="fullName" value="${patient.fullName}"
               minlength="5" maxlength="50" required>
    </div>
    <div class="form-group">
        <label for="address"><fmt:message key="patient.address"/></label>
        <input type="text" class="form-control" id="address" name="address" value="${patient.address}">
    </div>
    <div class="form-group">
        <label for="birthDate"><fmt:message key="patient.birthDate"/></label>
        <input type="date" class="form-control" id="birthDate" name="birthDate" value="${patient.birthDate}"
               required>
    </div>
    <div class="form-group">
        <label for="phones"><fmt:message key="patient.phones"/></label>
        <input type="text" class="form-control" id="phones" name="phones" value="${patient.phones}">
    </div>
    <div class="form-group">
        <label for="address"><fmt:message key="patient.email"/></label>
        <input type="email" class="form-control" id="email" name="email" value="${patient.email}">
    </div>

    <div class="form-group">
        <label for="receiptDate"><fmt:message key="patient.receiptDate"/></label>
        <input type="date" class="form-control" id="receiptDate" name="receiptDate"
               value="${patient.receiptDate}">
    </div>
    <div class="form-group">
        <label for="complaints"><fmt:message key="patient.complaints"/></label>
        <input type="text" class="form-control" id="complaints" name="complaints" value="${patient.complaints}">
    </div>
    <div class="form-group">
        <label for="diagnosis"><fmt:message key="patient.diagnosis"/></label>
        <input type="diagnosis" class="form-control" id="diagnosis" name="diagnosis" value="${patient.diagnosis}">
    </div>
    <div class="form-group">
        <label for="housing"><fmt:message key="patient.housing"/></label>
        <input type="text" class="form-control" id="housing" name="housing" value="${patient.housing}">
    </div>
    <div class="form-group">
        <label for="floor"><fmt:message key="patient.floor"/></label>
        <input type="text" class="form-control" id="floor" name="floor" value="${patient.floor}">
    </div>
    <div class="form-group">
        <label for="ward"><fmt:message key="patient.ward"/></label>
        <input type="text" class="form-control" id="ward" name="ward" value="${patient.ward}">
    </div>

<%--    <div class="form-group">--%>
<%--        <label for="nurse"><fmt:message key="patient.nurse"/></label>--%>
<%--        <input type="text" class="form-control" id="nurse" name="nurse" value="${patient.nurse}">--%>
<%--    </div>--%>
    <div class="form-group">
        <label for="exampleFormControlSelect2"><fmt:message key="patient.nurse"/></label>
        <select class="form-control" id="exampleFormControlSelect2" name="nurse">
            <c:forEach var="item" items="${listNurse}">
                <option value="${item.username}" ${item.username == patient.nurse ? 'selected' : ''}>${item.username}</option>
            </c:forEach>
        </select>
    </div>

<%--    <div class="form-group">--%>
<%--        <label for="cardStatus"><fmt:message key="patient.cardStatus"/></label>--%>
<%--        <input type="text" class="form-control" id="cardStatus" name="cardStatus" value="<spring:eval expression="patient.cardStatus"/>">--%>
<%--    </div>--%>

    <div class="form-group">
        <label for="exampleFormControlSelect1"><fmt:message key="patient.cardStatus"/></label>
        <select class="form-control" id="exampleFormControlSelect1" name="cardStatus">
            <c:forEach var="item" items="${listStatus}">
                <option value="${item}" ${item == patient.cardStatus ? 'selected' : ''}>${item} - <spring:eval expression="item"/></option>
            </c:forEach>
        </select>
    </div>


<%--    <button id="btn-save" type="submit" class="btn btn-primary addButton">Submit</button>--%>
    <%--    <button id="btn-save1" type="button" class="btn btn-primary addButton1">Save</button>--%>
<%--    <sec:authorize access="hasRole('ROLE_NURSEADMIN')">--%>
        <button id="btn-save2" type="submit" class="btn btn-outline-success my-2 my-sm-0 addButton2">Save</button>
        <button id="btn-reset" type="reset" class="btn btn-outline-success my-2 my-sm-0">Reset</button>
<%--    </sec:authorize>--%>

    <button id="cancel" type="button" class="btn btn-outline-success my-2 my-sm-0" onclick="javascript:window.location.href = '/patients'">Cancel</button>

    <div id="resultContainer" style="display: none;">
        <hr/>
        <h4 style="color: green;">JSON Response From Server</h4>
        <pre style="color: green;">
    <code></code>
   </pre>
    </div>
    <div style="color: red;" id="feedback"></div>
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>--%>

<%--<script  type="text/javascript">--%>
<%--    $(function() {--%>
<%--        /*  Submit form using Ajax */--%>
<%--        // $('button[type=submit]').click(function(e) {--%>
<%--        $('.addButton').click(function(e) {--%>
<%--            var d = $("#patientForm").serializeArray();--%>
<%--            console.log(d);--%>
<%--            var d1 = {}--%>
<%--            for (var i in d) {--%>
<%--                // console.log(" --- d1 : "  +  i + " : ");--%>
<%--                // console.log(d1[i]);--%>
<%--                // console.log(d1[i].name);--%>
<%--                // console.log(d1[i].value);--%>
<%--                d1[d[i].name] = d[i].value;--%>
<%--            }--%>
<%--            console.log("!!!!");--%>
<%--            console.log(d1);--%>
<%--            console.log("!!!!");--%>
<%--            //Prevent default submission of form--%>
<%--            e.preventDefault();--%>
<%--            //Remove all errors--%>
<%--            $('input').next().remove();--%>

<%--            $.post({--%>
<%--                url : '/api/v1/patients',--%>
<%--                // data : $('form[name=patientForm]').serialize(),--%>
<%--                data : JSON.stringify(d1),--%>
<%--                contentType: "application/json",--%>
<%--                dataType: "json",--%>
<%--                success : function(res) {--%>
<%--                    console.log(res);--%>
<%--                    if(res.validated){--%>
<%--                        //Set response--%>
<%--                        console.log(" --- SUCCESS");--%>
<%--                        $('#resultContainer pre code').text(JSON.stringify(res));--%>
<%--                        $('#resultContainer').show();--%>

<%--                    }else{--%>
<%--                        //Set error messages--%>
<%--                        $.each(res.errorMessages,function(key,value){--%>
<%--                            console.log(" --- error validate");--%>
<%--                            $('input[name='+key+']').after('<span class="error">'+value+'</span>');--%>
<%--                        });--%>
<%--                    }--%>
<%--                },--%>
<%--                error: function (e) {--%>

<%--                    console.log(e);--%>
<%--                    console.log(e.responseText);--%>
<%--                    var json = "<h4>Error save : </h4><pre>"--%>
<%--                        + e.responseText + "</pre>";--%>
<%--                    $('#feedback').html(json);--%>

<%--                    // console.log("ERROR : ", e);--%>
<%--                    // $("#btn-search").prop("disabled", false);--%>
<%--                    alert("Ошибка сохранения");--%>

<%--                }--%>

<%--            })--%>
<%--        });--%>
<%--    });--%>
<%--</script>--%>

<%--<script type="text/javascript">--%>
<%--    $(".addButton1").click(function (e) {--%>

<%--        var d = $("#patientForm").serializeArray();--%>
<%--        console.log(d);--%>
<%--        var d1 = {}--%>

<%--        for (var i in d) {--%>
<%--            // console.log(" --- d1 : "  +  i + " : ");--%>
<%--            // console.log(d1[i]);--%>
<%--            // console.log(d1[i].name);--%>
<%--            // console.log(d1[i].value);--%>
<%--            d1[d[i].name] = d[i].value;--%>
<%--        }--%>

<%--        console.log("!!!!");--%>
<%--        console.log(d1);--%>
<%--        console.log("!!!!");--%>
<%--        $.ajax({--%>
<%--            url: "/api/v1/patients",--%>
<%--            type: "POST",--%>
<%--            contentType: 'application/json;charset=UTF-8',--%>
<%--            // data: JSON.stringify({"housing": 1, "floor": 2,   "ward": 38}),--%>
<%--            data: JSON.stringify(d1),--%>
<%--            dataType: 'json',--%>
<%--            cache: false,--%>
<%--            timeout: 600000,--%>
<%--            success: function (response) {--%>
<%--                console.log(response);--%>
<%--                if (response == "1") {--%>
<%--                    location.reload();--%>
<%--                    alert("Выполнено");--%>
<%--                    // $("table.tablesorter").trigger('update');--%>
<%--                    // $("#dialog-create-reminder").modal("hide");--%>
<%--                } else {--%>
<%--                    alert("Ошибка");--%>
<%--                }--%>
<%--            }--%>
<%--        })--%>
<%--    })--%>
<%--</script>--%>

<script>
    $(document).ready(function () {

        $("#patientForm").submit(function (event) {

            //stop submit the form, we will post it manually.
            event.preventDefault();

            $("#btn-save2").prop("disabled", true);
            fire_ajax_submit();

        });

    });

    function fire_ajax_submit() {

        var search = {}
        search["username"] = $("#username").val();
        //search["email"] = $("#email").val();

        var d = $("#patientForm").serializeArray();
        console.log(d);
        var d1 = {}

        var type = "POST";

        for (var i in d) {
            // console.log(" --- d1 : "  +  i + " : ");
            // console.log(d1[i]);
            // console.log(d[i].name);

            // if(d[i].name == "id") {
            //     console.log(d[i].value);
            // }
            if(d[i].name == "id" && (d[i].value != "" || d[i].value >0 )){
                type = "PUT";
                // console.log(d[i].value);
                d1[d[i].name] = d[i].value;
            }
            // console.log(d1[i].value);
            if(d[i].name != "id"){
                d1[d[i].name] = d[i].value;
            }

        }
        console.log(" --- Type : " + type);

        console.log("!!!!");
        console.log(d1);
        console.log("!!!!");

        $("#btn-search").prop("disabled", true);

        $.ajax({
            // type: "POST",
            type: type,
            contentType: "application/json",
            url: "/api/v1/patients",
            data: JSON.stringify(d1),
            dataType: 'json',
            cache: false,
            timeout: 600000,
            success: function (data) {
                console.log(" --- success");
                console.log(data);

                // var json = "<h4>Ajax Response</h4><pre>"
                //     + JSON.stringify(data, null, 4) + "</pre>";
                // $('#feedback').html(json);
                //
                // console.log("SUCCESS : ", data);
                // $("#btn-search").prop("disabled", false);
                alert("Успешно сохранено");
                window.location.href = "/patients";

            },
            error: function (e) {

                console.log(e);
                var json = "<h4>Ajax Response</h4><pre>"
                    + e.responseText + "</pre>";
                // $('#feedback').html(json);

                console.log("ERROR : ", e);
                // $("#btn-search").prop("disabled", false);

                // console.log(e);
                // console.log(e.responseText);
                // var json = "<h4>Error save : </h4><pre>"
                //     + e.responseText + "</pre>";
                // $('#feedback').html(json);

                var errorText = "<h4>Error save : </h4><ul>";
                // + "<li>Чебурашка</li>\n" +
                // "    <li>Крокодил Гена</li>\n" +
                // "    <li>Шапокляк</li>"

                if(e.responseJSON === undefined){
                    errorText += "<li>" + e.responseText + "</li>";
                }
                else {
                    if(e.responseJSON.errors !== undefined) {
                        for (var i in e.responseJSON.errors) {
                            console.log(" --- e : " + i + " : ");
                            console.log(e.responseJSON.errors[i].field);
                            console.log(e.responseJSON.errors[i].defaultMessage);
                            errorText += "<li><strong>field: </strong>" + e.responseJSON.errors[i].field
                                + " <strong>message: </strong>" + e.responseJSON.errors[i].defaultMessage
                                + " <strong>value: </strong>" + e.responseJSON.errors[i].rejectedValue
                                + "</li>\n"
                            // console.log(d1[i].name);
                            // console.log(d1[i].value);
                            // d1[d[i].name] = d[i].value;
                        }
                    }else{
                        errorText += "<li>" + e.responseJSON.message + "</li>";
                    }
                }
                errorText += "</ul>";
                $('#feedback').html(errorText);

                $("#btn-save2").prop("disabled", false);

                // alert("Ошибка сохранения");

            }
        });

    }
</script>

<style type="text/css" media="screen">
    .error {
        border: 5px dashed red;
    }

    .success {
        border: 5px solid black;
    }
</style>

