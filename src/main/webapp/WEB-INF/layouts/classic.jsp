<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
<head>
    <title><tiles:getAsString name="title"/></title>
</head>




<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/axios@0.12.0/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/lodash@4.13.1/lodash.min.js"></script>


<body >
<div class="container" >
    <table width="100%">
        <tr>
            <td colspan="2">
                <tiles:insertAttribute name="header"/>
            </td>
        </tr>
        <tr>
            <td width="15%" nowrap="nowrap"  valign="top">
                <tiles:insertAttribute name="menu"/>
            </td>
            <td width="85%">
                <jsp:include page="/WEB-INF/components/body_componets.jsp" />
<%--                <%@include  file="/WEB-INF/components/body_componets.html" %>--%>
<%--                <jsp:include page="/WEB-INF/components/body_componets.html">--%>




                <tiles:insertAttribute name="body"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <tiles:insertAttribute name="footer"/>
            </td>
        </tr>
    </table>
</div>

<%--<div id="components">...</div>--%>

<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.1.0/css/flag-icon.min.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>



<div id='elementID'>

<script>
    fetch('component/test.component.html')
        .then(data => data.text())
    .then(html => document.getElementById('elementID').innerHTML = html);
</script>

<%--    <div id="app-test">--%>
<%--        {{ message }}--%>
<%--    </div>--%>


<%--<script src="component/test.component.js"></script>--%>

<script src="js/ui.component.loader.js"></script>

</div>




</body>
</html>
